package com.vironit.cleanarchitecture.mvp.presentation.presenter.base;

import android.support.annotation.Nullable;

import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBaseView;
import com.vironit.cleanarchitecture.util.AppLog;
import com.vironit.cleanarchitecture.util.throwable.CleanArchitectureBaseException;

import io.reactivex.functions.Consumer;

public abstract class BaseErrorPresenter<View extends IBaseView>
        extends BasePresenter<View> implements Consumer<Throwable> {

    @Override
    public final void accept(@Nullable Throwable throwable) throws Exception {
        if (throwable != null) {
            throwable.printStackTrace();
            AppLog.logPresenter(this, "Error handling", throwable);
        }
        defaultErrorHandling(throwable);
    }

    protected void handleUnknownError(@Nullable Throwable throwable) {
        getViewState().showAutocloseableMessage(getString(R.string.unknown_error));
    }

    protected void defaultErrorHandling(@Nullable Throwable throwable) {
        if (throwable != null && throwable instanceof Exception) {
            CleanArchitectureBaseException exception = (CleanArchitectureBaseException) throwable;
            if (exception.getErrorStatus() != null) {
                switch (exception.getErrorStatus()) {
                    default:
                        break;
                }
            } else if (exception.getStringResId() != null) {
                getViewState().showAutocloseableMessage(getString(exception.getStringResId()));
            }
        } else {
            handleUnknownError(throwable);
        }
    }
}
