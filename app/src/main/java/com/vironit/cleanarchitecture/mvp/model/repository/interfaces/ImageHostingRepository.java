package com.vironit.cleanarchitecture.mvp.model.repository.interfaces;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.images.CloudinaryResponse;

import io.reactivex.Single;

public interface ImageHostingRepository {

    Single<CloudinaryResponse> sendImage(String imageUrl);
}
