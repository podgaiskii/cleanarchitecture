package com.vironit.cleanarchitecture.mvp.presentation.presenter.base;

import android.support.annotation.Nullable;

import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBasePaginationView;
import com.vironit.cleanarchitecture.util.AppLog;

import java.util.List;

import io.reactivex.disposables.Disposable;

public abstract class BasePaginationPresenter<View extends IBasePaginationView>
        extends BaseAppPresenter<View> {

    protected boolean mIsNextPageAllowed = true;

    @Nullable
    private Disposable mPaginationDisposable;

    @Override
    protected void defaultErrorHandling(@Nullable Throwable throwable) {
        AppLog.logObject(this);
        getViewState().hidePaginationProgress();
        super.defaultErrorHandling(throwable);
    }

    @Override
    protected void clearLiteDisposable() {
        AppLog.logObject(this);
        clearPaginationDisposable();
        super.clearLiteDisposable();
    }

    protected void addPaginationDisposable(@Nullable Disposable disposable) {
        AppLog.logObject(this);
        if (disposable != null) {
            clearPaginationDisposable();
            mPaginationDisposable = disposable;
        }
    }

    protected void clearPaginationDisposable() {
        AppLog.logPresenter(this);
        if (isLoading()) {
            mPaginationDisposable.dispose();
        }
    }

    private boolean isLoading() {
        AppLog.logPresenter(this);
        return mPaginationDisposable != null && !mPaginationDisposable.isDisposed();
    }

    protected void loadData(int totalItemCount, String lastItemId) {
        AppLog.logPresenter(this);
        getViewState().showPaginationProgress();
    }

    public void loadCheckedData(int totalItemCount,
                                int lastVisibleItemPosition,
                                @Nullable String lastItemId) {
        AppLog.logPresenter(this);
        if (lastVisibleItemPosition + getItemCountPerPage() / 3 > totalItemCount
                && mIsNextPageAllowed
                && !isLoading()) {
            loadData(totalItemCount, lastItemId);
        }
    }

    protected int getItemCountPerPage() {
        AppLog.logPresenter(this);
        return AppConstants.DEFAULT_ITEM_COUNT_PER_PAGE;
    }

    public void refreshData() {
        AppLog.logPresenter(this);
        mIsNextPageAllowed = true;
        clearPaginationDisposable();
        getViewState().removeData();
        loadData(0, null);
    }

    protected void checkIfNextPageAllowed(@Nullable List list) {
        AppLog.logPresenter(this);
        mIsNextPageAllowed = !(list == null || (list.size() < getItemCountPerPage()));
    }
}
