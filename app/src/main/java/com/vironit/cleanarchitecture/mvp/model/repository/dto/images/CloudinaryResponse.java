package com.vironit.cleanarchitecture.mvp.model.repository.dto.images;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CloudinaryResponse {

    @SerializedName("public_id")
    @Expose
    private String publicId;

    @SerializedName("version")
    @Expose
    private Integer version;

    @SerializedName("signature")
    @Expose
    private String signature;

    @SerializedName("width")
    @Expose
    private Integer width;

    @SerializedName("height")
    @Expose
    private Integer height;

    @SerializedName("format")
    @Expose
    private String format;

    @SerializedName("resource_type")
    @Expose
    private String resourceType;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("tags")
    @Expose
    private List<String> tags = null;

    @SerializedName("bytes")
    @Expose
    private Integer bytes;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("etag")
    @Expose
    private String etag;

    @SerializedName("placeholder")
    @Expose
    private Boolean placeholder;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("secure_url")
    @Expose
    private String secureUrl;

    @SerializedName("original_filename")
    @Expose
    private String originalFilename;

    public String getPublicId() {
        return publicId;
    }

    public Integer getVersion() {
        return version;
    }

    public String getSignature() {
        return signature;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    public String getFormat() {
        return format;
    }

    public String getResourceType() {
        return resourceType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public List<String> getTags() {
        return tags;
    }

    public Integer getBytes() {
        return bytes;
    }

    public String getType() {
        return type;
    }

    public String getEtag() {
        return etag;
    }

    public Boolean getPlaceholder() {
        return placeholder;
    }

    public String getUrl() {
        return url;
    }

    public String getSecureUrl() {
        return secureUrl;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }
}
