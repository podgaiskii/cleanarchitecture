package com.vironit.cleanarchitecture.mvp.model.repository.db.entity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;

public class Insurance {

    @Nullable
    private Long insuranceId;
    @NonNull
    private Long insuranceCompanyId;
    @NonNull
    private String insuranceCompanyName;
    @NonNull
    private Date startDate;
    @NonNull
    private Date endDate;

    public Insurance(@Nullable Long insuranceId,
                     @NonNull Long insuranceCompanyId,
                     @NonNull String insuranceCompanyName,
                     @NonNull Date startDate,
                     @NonNull Date endDate) {
        this.insuranceId = insuranceId;
        this.insuranceCompanyId = insuranceCompanyId;
        this.insuranceCompanyName = insuranceCompanyName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Nullable
    public Long getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(@Nullable Long insuranceId) {
        this.insuranceId = insuranceId;
    }

    @NonNull
    public Long getInsuranceCompanyId() {
        return insuranceCompanyId;
    }

    public void setInsuranceCompanyId(@NonNull Long insuranceCompanyId) {
        this.insuranceCompanyId = insuranceCompanyId;
    }

    @NonNull
    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(@NonNull String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    @NonNull
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@NonNull Date startDate) {
        this.startDate = startDate;
    }

    @NonNull
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(@NonNull Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "InsuranceDB{" +
                "insuranceId=" + insuranceId +
                ", insuranceCompanyId=" + insuranceCompanyId +
                ", insuranceCompanyName='" + insuranceCompanyName + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
