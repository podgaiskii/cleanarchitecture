package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBaseView;

@StateStrategyType(SkipStrategy.class)
public interface IAuthView extends IBaseView {

    void startRegisterActivity();

    void startHomeActivity();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showSuccessMessage();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showFailMessage();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showEmptyFieldMessage();

    void setLogin(String login);
}
