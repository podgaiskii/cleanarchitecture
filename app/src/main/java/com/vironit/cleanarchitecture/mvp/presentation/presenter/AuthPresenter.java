package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vironit.cleanarchitecture.App;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.constants.AuthType;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.AuthInteractor;
import com.vironit.cleanarchitecture.mvp.model.repository.db.CleanArchitectureDatabase;
import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.DepartmentDB;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.User;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.AuthActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IAuthView;
import com.vironit.cleanarchitecture.util.AppLog;
import com.vironit.cleanarchitecture.util.SharedPreferencesUtil;
import com.vironit.cleanarchitecture.util.throwable.UserExistsException;
import com.vironit.cleanarchitecture.util.throwable.UserNotFoundException;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class AuthPresenter extends BaseAppPresenter<IAuthView> {

    @Inject
    AuthInteractor mAuthInteractor;

    @Inject
    CleanArchitectureDatabase mDatabase;

    @Inject
    CallbackManager mCallbackManager;

    @Inject
    TwitterAuthClient mTwitterAuthClient;

    @Inject
    GoogleSignInClient mGoogleSignInClient;

    @Inject
    DatabaseReference mDatabaseReference;

    @Inject
    SharedPreferences mSharedPreferences;

    private static int RC_SIGN_IN = 9001;

    public AuthPresenter() {
        App.getAppComponent().inject(this);
    }

    public void socialsAuthButtonPressed(@NonNull AuthType authType,
                                         @NonNull AuthActivity activity) {
        switch (authType) {
            case FACEBOOK:
                authViaFacebook(activity);
                break;
            case TWITTER:
                authViaTwitter(activity);
                break;
            case GOOGLE:
                authViaGoogle(activity);
                break;
        }
    }

    public void authButtonPressed(@NonNull AuthActivity activity,
                                  @Nullable String login,
                                  @Nullable String password) {
        if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)) {
            Disposable disposable = mAuthInteractor.authorizeUser(login, password)
                    .observeOn(mUiScheduler)
                    .subscribe(user -> onAuthSuccessful(activity, user),
                            throwable -> {
                                if (isUserExistsException(throwable)
                                        || isUserNotFoundException(throwable)) {
                                    getViewState().showDialogMessage(
                                            getString(R.string.wrong_login_or_password),
                                            true);
                                } else {
                                    getViewState().showDialogMessage(
                                            throwable.getMessage(),
                                            true);
                                }
                            });
        } else {
            getViewState().showEmptyFieldMessage();
        }
    }

    private boolean isUserExistsException(@NonNull Throwable throwable) {
        return throwable instanceof UserExistsException;
    }

    private boolean isUserNotFoundException(@NonNull Throwable throwable) {
        return throwable instanceof UserNotFoundException;
    }

    private void onSocialsAuthSuccessful(@NonNull AuthActivity activity,
                                         @NonNull Pair<String, String> userNameAndEmail) {
        if (!TextUtils.isEmpty(userNameAndEmail.second)) {
            activity.setLogin(userNameAndEmail.second);
        } else if (!TextUtils.isEmpty(userNameAndEmail.first)) {
            activity.setLogin(userNameAndEmail.first);
        }
    }

    @NonNull
    private Pair<String, String> getUserNameAndEmail(LoginResult facebookLoginResult) {
        List<String> list = new ArrayList<>();
        GraphRequest request = GraphRequest.newMeRequest(
                facebookLoginResult.getAccessToken(),
                (object, response) -> {
                    try {
                        list.addAll(Arrays.asList(object.getString("name"),
                                object.getString("email")));
                    } catch (JSONException e) {
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email");
        request.setParameters(parameters);
        Thread t = new Thread(request::executeAndWait);
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
        }
        return Pair.create(list.get(0), list.get(1));
    }

    private Pair<String, String> getUserNameAndEmail(Result<TwitterSession> twitterSessionResult) {
        return Pair.create(twitterSessionResult.data.getUserName(), null);
    }

    private Pair<String, String> getUserNameAndEmail(GoogleSignInAccount googleSignInAccount) {
        return Pair.create(googleSignInAccount.getDisplayName(), googleSignInAccount.getEmail());
    }

    private void onAuthSuccessful(@NonNull AuthActivity activity,
                                  @NonNull User user) {
        SharedPreferencesUtil.saveCurrentUser(mSharedPreferences, user);
        mAuthInteractor.setCurrentUserOnlineStatus(true)
                .subscribe();
        activity.startHomeActivity();
    }

    public void registerButtonPressed() {
        getViewState().startRegisterActivity();
    }

    private void authViaFacebook(@NonNull AuthActivity activity) {
        LoginManager.getInstance()
                .logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.i("AuthLog", loginResult.getAccessToken().getUserId());
                        onSocialsAuthSuccessful(activity, getUserNameAndEmail(loginResult));
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.i("AuthLog", error.toString());
                        getViewState().showFailMessage();
                    }
                });
    }

    private void authViaTwitter(@NonNull AuthActivity activity) {
        mTwitterAuthClient.authorize(activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.i("AuthLog", String.valueOf(result.data.getUserId()));
                onSocialsAuthSuccessful(activity, getUserNameAndEmail(result));
            }

            @Override
            public void failure(TwitterException exception) {
                Log.i("AuthLog", "failed");
                getViewState().showFailMessage();
            }
        });
    }

    private void authViaGoogle(@NonNull AuthActivity activity) {
        Intent intent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(intent, RC_SIGN_IN);
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask, AuthActivity activity) {
        try {
            Log.i("AuthLog", "ok");
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            onSocialsAuthSuccessful(activity, getUserNameAndEmail(account));
        } catch (ApiException e) {
            Log.i("AuthLog", "signInResult:failed code=" + e.getStatusCode());
            getViewState().showFailMessage();
        }
    }

    @Override
    public void detachView(IAuthView view) {
        super.detachView(view);
        LoginManager.getInstance().unregisterCallback(mCallbackManager);
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data,
                                 @NonNull BaseActivity activity) {
        super.onActivityResult(requestCode, resultCode, data, activity);

        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (mTwitterAuthClient != null) {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleSignInResult(task, (AuthActivity) activity);
        }
    }

    public void signOutFromAllAccounts() {
        LoginManager.getInstance().logOut();
        mGoogleSignInClient.signOut();
        if (TwitterCore.getInstance().getSessionManager().getActiveSession() != null) {
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
        }
    }

    public void dbRequest() {
        Single.fromCallable(() -> mDatabase.getDepartmentDao().insert(Arrays.asList(
                new DepartmentDB(1L, 1L, "department1"),
                new DepartmentDB(2L, 1L, "department2"),
                new DepartmentDB(3L, 1L, "department3")
        )))
                .subscribeOn(mIoScheduler)
                .subscribe();
        Disposable disposable = mDatabase.getCompanyDao()
                .getAllCompanyDepartments(1L)
                .subscribeOn(mIoScheduler)
                .subscribe(companyWithDepartmentsDB ->
                                AppLog.logPresenter(this, companyWithDepartmentsDB.toString()),
                        this);
    }
}
