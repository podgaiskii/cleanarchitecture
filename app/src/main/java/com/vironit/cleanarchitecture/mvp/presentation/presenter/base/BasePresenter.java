package com.vironit.cleanarchitecture.mvp.presentation.presenter.base;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.viewstate.MvpViewState;
import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.ResourcesManager;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBaseView;
import com.vironit.cleanarchitecture.util.AppLog;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<View extends IBaseView>
        extends MvpPresenter<View> implements ResourcesManager {

    @Inject
    @Named(AppConstants.UI_SCHEDULER)
    protected Scheduler mUiScheduler;

    @Inject
    @Named(AppConstants.IO_SCHEDULER)
    protected Scheduler mIoScheduler;

    @Inject
    @Named(AppConstants.COMPUTATION_SCHEDULER)
    protected Scheduler mComputationScheduler;

    @Inject
    ResourcesManager mResourcesManager;

    private final CompositeDisposable mLiteCompositeDisposable = new CompositeDisposable();
    private final CompositeDisposable mHardCompositeDisposable = new CompositeDisposable();

    protected void addLiteDisposable(@Nullable Disposable disposable) {
        if (disposable != null) {
            mLiteCompositeDisposable.add(disposable);
        }
    }

    protected void addHardDisposable(@Nullable Disposable disposable) {
        if (disposable != null) {
            mHardCompositeDisposable.add(disposable);
        }
    }

    protected void clearLiteDisposable() {
        mLiteCompositeDisposable.clear();
    }

    protected void clearHardDisposable() {
        mHardCompositeDisposable.clear();
    }

    @Override
    public void attachView(View view) {
        AppLog.logPresenter(this);
        super.attachView(view);
    }

    @Override
    protected void onFirstViewAttach() {
        AppLog.logPresenter(this);
        super.onFirstViewAttach();
    }

    @Override
    public void detachView(View view) {
        AppLog.logPresenter(this);
        clearLiteDisposable();
        super.detachView(view);
    }

    @Override
    public void setViewState(MvpViewState<View> viewState) {
        AppLog.logPresenter(this);
        super.setViewState(viewState);
    }

    @Override
    public void destroyView(View view) {
        AppLog.logPresenter(this);
        super.destroyView(view);
    }

    @Override
    public void onDestroy() {
        AppLog.logPresenter(this);
        clearHardDisposable();
        super.onDestroy();
    }


    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data,
                                 @NonNull BaseActivity activity) {
        AppLog.logPresenter(this);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults, @NonNull BaseActivity activity) {
        AppLog.logPresenter(this);
    }

    @NonNull
    @Override
    public String getString(int stringResId) {
        return mResourcesManager.getString(stringResId);
    }

    @NonNull
    @Override
    public String getString(int stringResId, @NonNull Object... formatArgs) {
        return mResourcesManager.getString(stringResId, formatArgs);
    }
}
