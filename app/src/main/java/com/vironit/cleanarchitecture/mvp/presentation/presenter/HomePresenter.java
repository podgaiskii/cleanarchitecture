package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import android.content.SharedPreferences;

import com.arellomobile.mvp.InjectViewState;
import com.vironit.cleanarchitecture.App;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.AuthInteractor;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IHomeView;
import com.vironit.cleanarchitecture.util.SharedPreferencesUtil;

import javax.inject.Inject;

@InjectViewState
public class HomePresenter extends BaseAppPresenter<IHomeView> {

    @Inject
    AuthInteractor mAuthInteractor;

    @Inject
    SharedPreferences mSharedPreferences;

    public HomePresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void onDestroy() {
        mAuthInteractor.setCurrentUserOnlineStatus(false)
                .subscribe();
        SharedPreferencesUtil.clearCurrentUser(mSharedPreferences);
        super.onDestroy();
    }
}