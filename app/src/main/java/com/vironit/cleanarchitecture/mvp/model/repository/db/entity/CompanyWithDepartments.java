package com.vironit.cleanarchitecture.mvp.model.repository.db.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;
import android.support.annotation.Nullable;

import java.util.List;

public class CompanyWithDepartments {

    @Nullable
    @Embedded
    private CompanyDB company;

    @Nullable
    @Relation(entity = DepartmentDB.class,
            parentColumn = CompanyDB.COMPANY_ID,
            entityColumn = CompanyDB.COMPANY_ID)
    private List<DepartmentDB> departments;

    @Nullable
    public CompanyDB getCompany() {
        return company;
    }

    public void setCompany(@Nullable CompanyDB company) {
        this.company = company;
    }

    @Nullable
    public List<DepartmentDB> getDepartments() {
        return departments;
    }

    public void setDepartments(@Nullable List<DepartmentDB> departments) {
        this.departments = departments;
    }

    @Override
    public String toString() {
        return "CompanyWithDepartmentsDB{" +
                "company=" + company +
                ", departments=" + departments.toString() +
                '}';
    }
}
