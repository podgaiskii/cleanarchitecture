package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

public interface IMessageView {

    void showAutocloseableMessage(String message);

    void showDialogMessage(String message, boolean closeable);

    void showDialogWithOptions(@Nullable String title,
                               @NonNull String message,
                               @Nullable String positiveOptionMessage,
                               @Nullable String negativeOptionMessage,
                               @Nullable DialogInterface.OnClickListener positiveOptionListener,
                               @Nullable DialogInterface.OnClickListener negativeOptionListener,
                               boolean cancelable);

    void showMessage(@NonNull String message, boolean closeable,
                     @Nullable String actionMessage, @Nullable View.OnClickListener onClickListener);

    void hideMessage();

    void hideDialogMessage();

    interface IActionListener {

        void onAction();
    }
}
