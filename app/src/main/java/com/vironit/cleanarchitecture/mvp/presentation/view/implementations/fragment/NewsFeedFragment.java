package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment;

import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Article;
import com.vironit.cleanarchitecture.mvp.presentation.adapter.NewsFeedAdapter;
import com.vironit.cleanarchitecture.mvp.presentation.adapter.base.BasePaginationRecyclerViewAdapter;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.NewsFeedPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.base.BasePaginationFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.INewsFeedView;
import com.vironit.cleanarchitecture.util.AppLog;

import java.util.List;

import butterknife.BindView;

public class NewsFeedFragment
        extends BasePaginationFragment<NewsFeedPresenter> implements INewsFeedView {

    @InjectPresenter
    NewsFeedPresenter mNewsFeedPresenter;

    private NewsFeedAdapter mNewsFeedAdapter;

    @BindView(R.id.l_f_news_feed_root)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.rv_news_feed)
    RecyclerView mRecyclerView;

    @Override
    protected NewsFeedPresenter getPresenter() {
        AppLog.logFragment(this);
        return mNewsFeedPresenter;
    }

    @NonNull
    @Override
    protected BasePaginationRecyclerViewAdapter getRecyclerViewAdapter() {
        AppLog.logFragment(this);
        return mNewsFeedAdapter;
    }

    @Override
    public int getLayoutResId() {
        AppLog.logFragment(this);
        return R.layout.fragment_news_feed;
    }

    public static NewsFeedFragment newInstance() {
        AppLog.logObject(NewsFeedFragment.class, null);
        return new NewsFeedFragment();
    }

    @NonNull
    @Override
    protected SwipeRefreshLayout getSwipeRefreshLayout() {
        AppLog.logFragment(this);
        return mSwipeRefreshLayout;
    }

    @NonNull
    @Override
    protected RecyclerView getRecyclerView() {
        AppLog.logFragment(this);
        return mRecyclerView;
    }

    @Override
    protected void setRecyclerViewAdapter() {
        AppLog.logFragment(this);
        if (getContext() != null) {
            mNewsFeedAdapter = new NewsFeedAdapter(getContext());
            mRecyclerView.setAdapter(mNewsFeedAdapter);
        }
    }

    @Override
    protected void setLayoutManager() {
        AppLog.logFragment(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void addData(List<Article> dataList) {
        AppLog.logFragment(this);
        mNewsFeedAdapter.addData(dataList);
    }
}
