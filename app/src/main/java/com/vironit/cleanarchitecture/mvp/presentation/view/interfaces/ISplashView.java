package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces;

import com.arellomobile.mvp.MvpView;

public interface ISplashView extends MvpView {

    void navigateToLoginScreen();
}
