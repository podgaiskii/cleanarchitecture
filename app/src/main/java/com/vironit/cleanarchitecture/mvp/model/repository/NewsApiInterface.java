package com.vironit.cleanarchitecture.mvp.model.repository;

import android.support.annotation.IntRange;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Data;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApiInterface {

    @GET("top-headlines")
    Single<Data> getNews(@Query("country") String countryCode,
                         @Query("page") @IntRange(from = 1) int page,
                         @Query("pageSize") @IntRange(from = 1, to = 100) int pageSize);
}
