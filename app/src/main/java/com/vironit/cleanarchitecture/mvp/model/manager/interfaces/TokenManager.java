package com.vironit.cleanarchitecture.mvp.model.manager.interfaces;

import android.support.annotation.Nullable;

import com.vironit.cleanarchitecture.mvp.model.manager.implementations.TokenManagerImpl;

import java.util.concurrent.atomic.AtomicLong;

public interface TokenManager {

    @Nullable
    TokenManagerImpl.ApiToken getToken();

    void saveToken(AtomicLong creationDate, String accessToken, String refreshToken);

    void deleteToken();

    boolean isTokenValid();
}
