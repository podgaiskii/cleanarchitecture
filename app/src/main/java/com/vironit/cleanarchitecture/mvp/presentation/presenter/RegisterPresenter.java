package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vironit.cleanarchitecture.App;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.constants.AuthType;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.AuthInteractor;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.User;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.RegisterActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IRegisterView;
import com.vironit.cleanarchitecture.util.SharedPreferencesUtil;
import com.vironit.cleanarchitecture.util.throwable.DatabaseErrorException;
import com.vironit.cleanarchitecture.util.throwable.UserExistsException;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class RegisterPresenter extends BaseAppPresenter<IRegisterView> {

    @Inject
    AuthInteractor mAuthInteractor;

    @Inject
    CallbackManager mCallbackManager;

    @Inject
    TwitterAuthClient mTwitterAuthClient;

    @Inject
    GoogleSignInClient mGoogleSignInClient;

    @Inject
    SharedPreferences mSharedPreferences;

    private static int RC_SIGN_IN = 9001;

    public RegisterPresenter() {
        App.getAppComponent().inject(this);
    }

    public void registerButtonPressed(@Nullable String userName,
                                      @Nullable String email,
                                      @Nullable String password,
                                      @Nullable String passwordConfirm,
                                      @NonNull RegisterActivity activity) {
        if (TextUtils.isEmpty(userName) ||
                TextUtils.isEmpty(email) ||
                TextUtils.isEmpty(password) ||
                TextUtils.isEmpty(passwordConfirm)) {
            getViewState().showDialogMessage(
                    getString(R.string.all_fields_must_be_filled), true);
        } else if (!isPasswordConfirmed(password, passwordConfirm)) {
            getViewState().showDialogMessage(getString(R.string.passwords_mismatch), true);
        } else {
            Disposable disposable = mAuthInteractor.registerUser(userName, email, password)
                    .observeOn(mUiScheduler)
                    .subscribe(user -> onRegisterSuccessful(activity, user),
                            throwable -> {
                                if (isUserExistsException(throwable)) {
                                    getViewState().showDialogMessage(
                                            getString(R.string.name_or_email_already_exists),
                                            true);
                                } else if (isDatabaseErrorException(throwable)) {
                                    getViewState().showDialogMessage(
                                            getString(R.string.database_error),
                                            true);
                                } else {
                                    getViewState().showDialogMessage(
                                            getMessageFromThrowable(throwable),
                                            true);
                                }
                            });
        }
    }

    private void onRegisterSuccessful(@NonNull RegisterActivity activity,
                                      @NonNull User user) {
        SharedPreferencesUtil.saveCurrentUser(mSharedPreferences, user);
        activity.startHomeActivity();
    }

    public void socialsAuthButtonPressed(@NonNull AuthType authType,
                                         @NonNull RegisterActivity activity) {
        switch (authType) {
            case FACEBOOK:
                authViaFacebook(activity);
                break;
            case TWITTER:
                authViaTwitter(activity);
                break;
            case GOOGLE:
                authViaGoogle(activity);
                break;
        }
    }

    private void authViaFacebook(@NonNull RegisterActivity activity) {
        LoginManager.getInstance()
                .logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.i("AuthLog", loginResult.getAccessToken().getUserId());
                        onSocialsAuthSuccessful(activity, getUserNameAndEmail(loginResult));
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.i("AuthLog", error.toString());
                        getViewState().showFailMessage();
                    }
                });
    }

    private void authViaTwitter(@NonNull RegisterActivity activity) {
        mTwitterAuthClient.authorize(activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.i("AuthLog", String.valueOf(result.data.getUserId()));
                onSocialsAuthSuccessful(activity, getUserNameAndEmail(result));
            }

            @Override
            public void failure(TwitterException exception) {
                Log.i("AuthLog", "failed");
                getViewState().showFailMessage();
            }
        });
    }

    private void authViaGoogle(@NonNull RegisterActivity activity) {
        Intent intent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(intent, RC_SIGN_IN);
    }

    private void onSocialsAuthSuccessful(@NonNull RegisterActivity activity,
                                         @NonNull Pair<String, String> userNameAndEmail) {
        activity.setCredentials(userNameAndEmail.first, userNameAndEmail.second);
    }

    @NonNull
    private Pair<String, String> getUserNameAndEmail(LoginResult facebookLoginResult) {
        List<String> list = new ArrayList<>();
        GraphRequest request = GraphRequest.newMeRequest(
                facebookLoginResult.getAccessToken(),
                (object, response) -> {
                    try {
                        list.addAll(Arrays.asList(object.getString("name"),
                                object.getString("email")));
                    } catch (JSONException e) {
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email");
        request.setParameters(parameters);
        Thread t = new Thread(request::executeAndWait);
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
        }
        return Pair.create(list.get(0), list.get(1));
    }

    private Pair<String, String> getUserNameAndEmail(Result<TwitterSession> twitterSessionResult) {
        return Pair.create(twitterSessionResult.data.getUserName(), null);
    }

    private Pair<String, String> getUserNameAndEmail(GoogleSignInAccount googleSignInAccount) {
        return Pair.create(googleSignInAccount.getDisplayName(), googleSignInAccount.getEmail());
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data,
                                 @NonNull BaseActivity activity) {
        super.onActivityResult(requestCode, resultCode, data, activity);

        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (mTwitterAuthClient != null) {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleSignInResult(task, (RegisterActivity) activity);
        }
    }

    private void handleGoogleSignInResult(@NonNull Task<GoogleSignInAccount> completedTask,
                                          @NonNull RegisterActivity activity) {
        try {
            Log.i("AuthLog", "ok");
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            onSocialsAuthSuccessful(activity, getUserNameAndEmail(account));
        } catch (ApiException e) {
            Log.i("AuthLog", "googleSignInResult:failed code=" + e.getStatusCode());
            getViewState().showFailMessage();
        }
    }

    private boolean isPasswordConfirmed(@NonNull String password,
                                        @NonNull String passwordConfirm) {
        return password.equals(passwordConfirm);
    }

    private boolean isUserExistsException(@NonNull Throwable throwable) {
        return throwable instanceof UserExistsException;
    }

    private boolean isDatabaseErrorException(@NonNull Throwable throwable) {
        return throwable instanceof DatabaseErrorException;
    }

    private String getMessageFromThrowable(@NonNull Throwable throwable) {
        return getString(R.string.unknown_error) + ':' + throwable.getMessage();
    }
}
