package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces;

import android.support.annotation.Nullable;

import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBaseView;

public interface IRegisterView extends IBaseView {


    void startHomeActivity();

    void setCredentials(@Nullable String userName, @Nullable String email);

    void showSuccessMessage();

    void showFailMessage();

    void showEmptyFieldMessage();
}
