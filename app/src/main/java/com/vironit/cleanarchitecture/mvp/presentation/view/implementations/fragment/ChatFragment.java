package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.ChatPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.base.BaseFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IChatView;

public class ChatFragment extends BaseFragment<ChatPresenter> implements IChatView {

    @InjectPresenter
    ChatPresenter mChatPresenter;

    @Override
    protected ChatPresenter getPresenter() {
        return mChatPresenter;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_chat;
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }
}
