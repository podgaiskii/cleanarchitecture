package com.vironit.cleanarchitecture.mvp.model.interactor.interfaces;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.images.CloudinaryResponse;

import io.reactivex.Single;

public interface ImageHostingInteractor {

    Single<CloudinaryResponse> sendImage(String imageUrl);
}
