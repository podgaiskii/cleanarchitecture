package com.vironit.cleanarchitecture.mvp.model.repository.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

@Entity(tableName = CompanyDB.COMPANY_TABLE_NAME)
public class CompanyDB {

    public static final String COMPANY_TABLE_NAME = "companies";
    public static final String COMPANY_ID = "company_id";
    public static final String COMPANY_TABLE_COLUMNS =
            COMPANY_TABLE_NAME + "." + COMPANY_ID + ", " +
                    COMPANY_TABLE_NAME + ".company_name, " +
                    COMPANY_TABLE_NAME + ".salaries";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COMPANY_ID)
    @Nullable
    private Long companyId;

    @ColumnInfo(name = "company_name")
    @NonNull
    private String companyName;

    @ColumnInfo(name = "salaries")
    @NonNull
    private List<Long> salaries;

    public CompanyDB(@Nullable Long companyId,
                     @NonNull String companyName,
                     @NonNull List<Long> salaries) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.salaries = salaries;
    }

    @Nullable
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(@Nullable Long companyId) {
        this.companyId = companyId;
    }

    @NonNull
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(@NonNull String companyName) {
        this.companyName = companyName;
    }

    @NonNull
    public List<Long> getSalaries() {
        return salaries;
    }

    public void setSalaries(@NonNull List<Long> salaries) {
        this.salaries = salaries;
    }

    @Override
    public String toString() {
        return "CompanyDB{" +
                "companyId=" + companyId +
                ", companyName='" + companyName + '\'' +
                ", salaries=" + salaries.toString() +
                '}';
    }
}
