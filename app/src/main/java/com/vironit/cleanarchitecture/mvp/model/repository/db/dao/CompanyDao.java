package com.vironit.cleanarchitecture.mvp.model.repository.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.CompanyDB;
import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.CompanyWithDepartments;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface CompanyDao {

    @Query("SELECT * FROM " + CompanyDB.COMPANY_TABLE_NAME)
    Single<CompanyDB> getAll();

    @Query("SELECT * FROM " + CompanyDB.COMPANY_TABLE_NAME + " WHERE company_id = :id")
    Single<CompanyDB> getById(long id);

    @Query("SELECT " + CompanyDB.COMPANY_TABLE_COLUMNS + " FROM " + CompanyDB.COMPANY_TABLE_NAME +
            " WHERE " + CompanyDB.COMPANY_ID + " = :id")
    Single<CompanyWithDepartments> getAllCompanyDepartments(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert(List<CompanyDB> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(CompanyDB company);

    @Delete
    int delete(List<CompanyDB> list);

    @Delete
    int delete(CompanyDB company);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(List<CompanyDB> list);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(CompanyDB company);
}
