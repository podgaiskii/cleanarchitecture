package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.constants.AuthType;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.AuthPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IAuthView;

import butterknife.BindView;
import butterknife.OnClick;

public class AuthActivity extends BaseActivity<AuthPresenter> implements IAuthView {

    @InjectPresenter
    AuthPresenter mAuthPresenter;

    @BindView(R.id.et_login)
    EditText etLogin;

    @BindView(R.id.et_password)
    EditText etPassword;

    public static void start(@Nullable Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, AuthActivity.class));
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuthPresenter.signOutFromAllAccounts();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mAuthPresenter.dbRequest();
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_auth;
    }

    @Override
    public int getRootViewResId() {
        return R.id.v_root_auth_activity;
    }

    protected AuthPresenter getPresenter() {
        return mAuthPresenter;
    }

    @OnClick({R.id.btn_auth, R.id.btn_register,
            R.id.btn_facebook_auth, R.id.btn_twitter_auth, R.id.btn_google_auth})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_facebook_auth:
                mAuthPresenter.socialsAuthButtonPressed(AuthType.FACEBOOK, this);
                break;
            case R.id.btn_twitter_auth:
                mAuthPresenter.socialsAuthButtonPressed(AuthType.TWITTER, this);
                break;
            case R.id.btn_google_auth:
                mAuthPresenter.socialsAuthButtonPressed(AuthType.GOOGLE, this);
                break;
            case R.id.btn_auth:
                mAuthPresenter.authButtonPressed(this,
                        etLogin.getText().toString(),
                        etPassword.getText().toString());
                break;
            case R.id.btn_register:
                mAuthPresenter.registerButtonPressed();
                break;
        }
    }

    @Override
    public void startRegisterActivity() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @Override
    public void startHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    @Override
    public void showSuccessMessage() {
        showDialogMessage(getString(R.string.successful_entry_msg), true);
    }

    @Override
    public void showFailMessage() {
        showDialogMessage(getString(R.string.failed_entry_msg), true);
    }

    @Override
    public void showEmptyFieldMessage() {
        showDialogMessage(getString(R.string.all_fields_must_be_filled), true);
    }

    @Override
    public void setLogin(@NonNull String login) {
        etLogin.setText(login);
        etPassword.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
}
