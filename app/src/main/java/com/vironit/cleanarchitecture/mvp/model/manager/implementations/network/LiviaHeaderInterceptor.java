package com.vironit.cleanarchitecture.mvp.model.manager.implementations.network;

import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.TokenManager;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class LiviaHeaderInterceptor implements Interceptor {

    @Inject
    TokenManager mTokenManager;

    @Inject
    LiviaHeaderInterceptor(TokenManager mTokenManager) {
        this.mTokenManager = mTokenManager;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (mTokenManager.isTokenValid()) {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder();
            requestBuilder.header("liviaapp-token", mTokenManager.getToken().getAccessToken());
            return chain.proceed(requestBuilder.build());
        }
        return null;
    }
}
