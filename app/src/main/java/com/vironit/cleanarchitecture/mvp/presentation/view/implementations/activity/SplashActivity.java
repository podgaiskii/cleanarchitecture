package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.SplashPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.ISplashView;

public class SplashActivity extends MvpAppCompatActivity implements ISplashView {

    @InjectPresenter
    SplashPresenter splashPresenter;

    @Override
    public void navigateToLoginScreen() {
        AuthActivity.start(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashPresenter.navigateToLogin();
    }
}
