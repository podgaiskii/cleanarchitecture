package com.vironit.cleanarchitecture.mvp.model.repository.implementations;

import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.repository.ImageHostingApiInterface;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.images.CloudinaryRequestBody;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.images.CloudinaryResponse;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.ImageHostingRepository;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ImageHostingRepositoryImpl implements ImageHostingRepository {

    private final ImageHostingApiInterface mImageHostingApiInterface;
    private final Scheduler mIOScheduler;

    @Inject
    ImageHostingRepositoryImpl(ImageHostingApiInterface imageHostingApiInterface,
                               @Named(AppConstants.IO_SCHEDULER) Scheduler scheduler) {
        this.mImageHostingApiInterface = imageHostingApiInterface;
        this.mIOScheduler = scheduler;
    }

    @Override
    public Single<CloudinaryResponse> sendImage(String imageUrl) {
        CloudinaryRequestBody requestBody = new CloudinaryRequestBody(imageUrl);
        return sendImageRequest(requestBody);
    }

    private Single<CloudinaryResponse> sendImageRequest(CloudinaryRequestBody requestBody) {
        return mImageHostingApiInterface.sendImage(
                getFilePart(requestBody.getFile()),
                getStringPart("api_key", requestBody.getApiKey()),
                getStringPart("timestamp", requestBody.getTimestamp()),
                getStringPart("signature", requestBody.getSignature()))
                .subscribeOn(mIOScheduler);
    }

    private MultipartBody.Part getFilePart(File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData("file", file.getName(), requestFile);
    }

    private MultipartBody.Part getStringPart(String name, String value) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), value);
        return MultipartBody.Part.createFormData(name, value);
    }
}
