package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBaseView;

public interface IProfileView extends IBaseView {

    void setProfilePhoto(@NonNull String path);

    void setProfilePhoto(@NonNull Uri uri);

    void setProfileName(@Nullable String name);

    void setProfileEmail(@Nullable String email);
}
