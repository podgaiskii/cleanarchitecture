package com.vironit.cleanarchitecture.mvp.model.manager.firebase.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.SplashActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final int CODE = 13113;
    private static final String CHANNEL_ID = "com.vironit.cleanarchitecture.notification";
    private static final String CHANNEL_NAME = "Clean Architecture Notification";
    private static final String CHANNEL_DESCRIPTION = "Clean Architecture Notification Channel";

    private static final int TYPE_IMAGE = 1;
    private static final int TYPE_TEXT = 2;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String type = remoteMessage.getData().get("type");
        String imageUrl = type.equals("image") ? remoteMessage.getData().get("image_url") : "";
        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("message");
        int notificationType = type.equals("image") ? TYPE_IMAGE : TYPE_TEXT;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.leak_canary_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(PendingIntent.getActivity(this, CODE,
                        new Intent(this, SplashActivity.class),
                        PendingIntent.FLAG_UPDATE_CURRENT))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        switch (notificationType) {
            case TYPE_IMAGE:
                showImage(builder, title, message, imageUrl);
                break;
            case TYPE_TEXT:
                showText(builder, title, message);
                break;
        }
    }

    private void showText(NotificationCompat.Builder builder,
                          String title,
                          String message) {
        builder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(message)
                .setBigContentTitle(title));
        notify(builder, this);
    }

    private void showImage(NotificationCompat.Builder builder,
                           String title,
                           String message,
                           String imageUrl) {
        Handler mainHandler = new Handler(getApplicationContext().getMainLooper());
        Runnable runnable = () -> {
            Glide.with(this)
                    .asBitmap()
                    .load(imageUrl)
                    .into(new SimpleTarget<Bitmap>(300, 200) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            builder.setStyle(new NotificationCompat.BigPictureStyle()
                                    .bigPicture(resource)
                                    .setBigContentTitle(title)
                                    .setSummaryText(message));
                            MyFirebaseMessagingService.notify(builder,
                                    MyFirebaseMessagingService.this);
                            builder.setLargeIcon(resource);
                        }
                    });
        };
        mainHandler.post(runnable);
    }

    private static void notify(NotificationCompat.Builder builder, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = context.getSystemService(NotificationManager.class);
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);

            final long[] vibrationPattern = {0, 500, 500, 1000};

            channel.setDescription(CHANNEL_DESCRIPTION);
            channel.enableLights(true);
            channel.setLightColor(R.color.colorAccent);
            channel.setVibrationPattern(vibrationPattern);
            channel.enableVibration(true);

            builder.setVibrate(vibrationPattern);

            manager.createNotificationChannel(channel);
            manager.notify(CODE, builder.build());
        } else {
            NotificationManagerCompat manager = NotificationManagerCompat.from(context);
            manager.notify(CODE, builder.build());
        }
    }
}
