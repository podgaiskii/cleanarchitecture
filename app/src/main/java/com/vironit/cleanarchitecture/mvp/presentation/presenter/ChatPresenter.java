package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.vironit.cleanarchitecture.App;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IChatView;

@InjectViewState
public class ChatPresenter extends BaseAppPresenter<IChatView> {

    public ChatPresenter() {
        App.getAppComponent().inject(this);
    }
}
