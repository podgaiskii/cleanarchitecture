package com.vironit.cleanarchitecture.mvp.model.repository.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;

@Entity(tableName = EmployeeDB.EMPLOYEE_TABLE_NAME,
        foreignKeys = {@ForeignKey(
                entity = DepartmentDB.class,
                parentColumns = DepartmentDB.DEPARTMENT_ID,
                childColumns = DepartmentDB.DEPARTMENT_ID,
                onDelete = ForeignKey.CASCADE,
                onUpdate = ForeignKey.CASCADE)},
        indices = {@Index(
                value = EmployeeDB.COLUMN_LAST_NAME,
                unique = false,
                name = "EMPLOYEE_LAST_NAME")})
public class EmployeeDB {

    public static final String EMPLOYEE_TABLE_NAME = "employees";
    public static final String EMPLOYEE_ID = "employee_id";
    public static final String COLUMN_LAST_NAME = "last_name";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = EMPLOYEE_ID)
    @Nullable
    private Long employeeId;

    @ColumnInfo(name = "department_id")
    @NonNull
    private Long departmentId;

    @ColumnInfo(name = "first_name")
    @NonNull
    private String firstName;

    @ColumnInfo(name = "middle_name")
    @NonNull
    private String middleName;

    @ColumnInfo(name = COLUMN_LAST_NAME)
    @NonNull
    private String lastName;

    @Embedded
    @NonNull
    private Insurance insurance;

    @ColumnInfo(name = "birth_date")
    @NonNull
    private Date birthDate;

    public EmployeeDB(@Nullable Long employeeId,
                      @NonNull Long departmentId,
                      @NonNull String firstName,
                      @NonNull String middleName,
                      @NonNull String lastName,
                      @NonNull Insurance insurance,
                      @NonNull Date birthDate) {
        this.employeeId = employeeId;
        this.departmentId = departmentId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.insurance = insurance;
        this.birthDate = birthDate;
    }

    @Nullable
    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(@Nullable Long employeeId) {
        this.employeeId = employeeId;
    }

    @NonNull
    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(@NonNull Long departmentId) {
        this.departmentId = departmentId;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NonNull String firstName) {
        this.firstName = firstName;
    }

    @NonNull
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(@NonNull String middleName) {
        this.middleName = middleName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@NonNull String lastName) {
        this.lastName = lastName;
    }

    @NonNull
    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(@NonNull Insurance insurance) {
        this.insurance = insurance;
    }

    @NonNull
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(@NonNull Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "EmployeeDB{" +
                "employeeId=" + employeeId +
                ", departmentId=" + departmentId +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", insurance=" + insurance.toString() +
                ", birthDate=" + birthDate.toString() +
                '}';
    }
}
