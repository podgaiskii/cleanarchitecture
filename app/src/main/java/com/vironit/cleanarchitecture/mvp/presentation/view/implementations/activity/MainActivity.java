package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity;

import android.os.Bundle;

import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.MainPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IMainView;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainView {

    @Inject
    protected MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainPresenter.showProgressWithMessage(getString(R.string.app_name));
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public int getRootViewResId() {
        return R.id.v_root_main_activity;
    }

    protected MainPresenter getPresenter() {
        return mMainPresenter;
    }
}
