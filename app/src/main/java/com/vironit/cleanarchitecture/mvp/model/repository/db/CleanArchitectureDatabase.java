package com.vironit.cleanarchitecture.mvp.model.repository.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.vironit.cleanarchitecture.mvp.model.repository.db.converter.DateToLongConverter;
import com.vironit.cleanarchitecture.mvp.model.repository.db.converter.ListLongToStringConverter;
import com.vironit.cleanarchitecture.mvp.model.repository.db.dao.CompanyDao;
import com.vironit.cleanarchitecture.mvp.model.repository.db.dao.DepartmentDao;
import com.vironit.cleanarchitecture.mvp.model.repository.db.dao.EmployeeDao;
import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.CompanyDB;
import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.DepartmentDB;
import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.EmployeeDB;

@Database(entities = {CompanyDB.class, DepartmentDB.class, EmployeeDB.class}, version = 1)
@TypeConverters({DateToLongConverter.class, ListLongToStringConverter.class})
public abstract class CleanArchitectureDatabase extends RoomDatabase {

    public abstract CompanyDao getCompanyDao();

    public abstract DepartmentDao getDepartmentDao();

    public abstract EmployeeDao getEmployeeDao();
}
