package com.vironit.cleanarchitecture.mvp.model.repository.db.converter;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.Nullable;

import java.util.Date;

public class DateToLongConverter {

    @TypeConverter
    @Nullable
    public Date longToDate(@Nullable Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    @Nullable
    public Long dateToLong(@Nullable Date value) {
        return value == null ? null : value.getTime();
    }
}
