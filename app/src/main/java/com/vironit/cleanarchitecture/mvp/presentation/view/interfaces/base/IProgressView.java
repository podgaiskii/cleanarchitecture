package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base;

public interface IProgressView {

    void showProgress();

    void showProgress(String message);

    void hideProgress();
}
