package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.vironit.cleanarchitecture.App;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.ImageHostingInteractor;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.images.CloudinaryResponse;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.User;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.base.BaseFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IProfileView;
import com.vironit.cleanarchitecture.util.AppLog;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

@InjectViewState
public class ProfilePresenter extends BaseAppPresenter<IProfileView> {

    private static final String FIELD_PHOTO_URL = "photoUrl";

    @Inject
    ImageHostingInteractor mImageHostingInteractor;

    @Inject
    SharedPreferences mSharedPreferences;

    private static final int REQUEST_IMAGE_CAPTURE_CAMERA = 1;
    private static final int REQUEST_IMAGE_CAPTURE_GALLERY = 2;
    private static final int REQUEST_CAMERA_PERMISSION = 3;
    private static final int REQUEST_GALLERY_PERMISSION = 4;
    private static final int REQUEST_OPEN_SETTINGS = 5;

    private static BaseFragment mRootFragment;

    private String imageFilePath = "";

    public ProfilePresenter() {
        App.getAppComponent().inject(this);
    }

    public void uploadPhotoFromCamera(BaseFragment fragment) {
        mRootFragment = fragment;
        if (isPermissionGranted(Manifest.permission.CAMERA)) {
            openCamera();
        } else {
            requestPermission(
                    Manifest.permission.CAMERA,
                    REQUEST_CAMERA_PERMISSION,
                    getString(R.string.camera));
        }
    }

    public void uploadPhotoFromGallery(BaseFragment fragment) {
        mRootFragment = fragment;
        if (isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            openGallery();
        } else {
            requestPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    REQUEST_GALLERY_PERMISSION,
                    getString(R.string.gallery));
        }
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri outputUri = createImageUri();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        mRootFragment.startActivityForResult(intent, REQUEST_IMAGE_CAPTURE_CAMERA);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        mRootFragment.startActivityForResult(
                Intent.createChooser(intent, getString(R.string.select_file)),
                REQUEST_IMAGE_CAPTURE_GALLERY);
    }

    private Uri createImageUri() {
        Uri uri;
        File file = null;
        try {
            file = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(
                    mRootFragment.getContext(),
                    mRootFragment.getActivity().getApplicationContext().
                            getPackageName() + ".provider",

                    file);
        } else {
            uri = Uri.fromFile(file);
        }
        return uri;
    }

    private File createImageFile() throws IOException {
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        String imageFileName = "IMG_" + time + "_";
        File storageDir =
                mRootFragment.getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        imageFilePath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults,
                                           @NonNull BaseActivity activity) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera();
            }
        } else if (requestCode == REQUEST_GALLERY_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults, activity);
        }
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data,
                                 @NonNull BaseActivity activity) {
        super.onActivityResult(requestCode, resultCode, data, activity);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE_CAPTURE_CAMERA:
                    uploadPhoto(imageFilePath);
                    break;
                case REQUEST_IMAGE_CAPTURE_GALLERY:
                    uploadPhoto(getPath(data.getData()));
                    break;
            }
        }
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(mRootFragment.getContext(), uri, projection, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(mRootFragment.getContext(), permission) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(String permission, int requestCode, String permissionName) {
        if (!mRootFragment.shouldShowRequestPermissionRationale(permission)) {
            getViewState().showMessage(
                    getString(R.string.permission_is_not_allowed) + permissionName,
                    false,
                    getString(R.string.settings),
                    v -> openAppSettings()
            );
        } else {
            mRootFragment.requestPermissions(new String[]{permission}, requestCode);
        }
    }

    private void openAppSettings() {
        Intent intent = new Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + mRootFragment.getActivity().getPackageName()));
        mRootFragment.startActivityForResult(intent, REQUEST_OPEN_SETTINGS);
    }

    private void uploadPhoto(String path) {
        Log.e("PHOTO_PATH", path);
        addLiteDisposable(mImageHostingInteractor.sendImage(path)
                .observeOn(mUiScheduler)
                .doOnSuccess(this::parseCloudinaryResponse)
                .subscribe(list -> AppLog.logPresenter(this),
                        this));
    }

    private void parseCloudinaryResponse(CloudinaryResponse response) {
        mSharedPreferences.edit()
                .putString(FIELD_PHOTO_URL, response.getUrl())
                .apply();
        getViewState().setProfilePhoto(response.getUrl());
    }

    public void setProfileInfo() {
        getViewState().setProfileEmail(mSharedPreferences.getString(User.FIELD_USER_EMAIL, null));
        getViewState().setProfileName(mSharedPreferences.getString(User.FIELD_USER_NAME, null));
    }

    public void setDefaultProfilePhoto() {
        String defaultPhotoUrl = mSharedPreferences.getString(FIELD_PHOTO_URL, null);
        if (defaultPhotoUrl != null) {
            getViewState().setProfilePhoto(defaultPhotoUrl);
        }
    }
}
