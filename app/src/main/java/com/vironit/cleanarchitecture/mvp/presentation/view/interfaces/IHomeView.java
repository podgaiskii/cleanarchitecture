package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces;

import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.constants.HomeFragmentType;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBaseView;

public interface IHomeView extends IBaseView {

    void showFragment(@NonNull HomeFragmentType fragmentType);
}
