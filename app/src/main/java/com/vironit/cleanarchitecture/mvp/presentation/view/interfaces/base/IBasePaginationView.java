package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(SkipStrategy.class)
public interface IBasePaginationView extends IBaseView {

    void removeData();

    void showPaginationProgress();

    void hidePaginationProgress();
}
