package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.ISplashView;

import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

@InjectViewState
public class SplashPresenter extends MvpPresenter<ISplashView> {

    private PublishSubject<Integer> subject = PublishSubject.create();

    private Disposable disposable = subject.delay(AppConstants.SPLASH_SCREEN_DELAY, TimeUnit.MILLISECONDS)
            .subscribe(integer -> getViewState().navigateToLoginScreen());

    public void navigateToLogin() {
        subject.onNext(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
