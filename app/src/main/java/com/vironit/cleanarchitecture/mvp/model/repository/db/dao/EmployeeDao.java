package com.vironit.cleanarchitecture.mvp.model.repository.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.EmployeeDB;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface EmployeeDao {

    @Query("SELECT * FROM " + EmployeeDB.EMPLOYEE_TABLE_NAME)
    Single<List<EmployeeDB>> getAll();

    @Query("SELECT * FROM " + EmployeeDB.EMPLOYEE_TABLE_NAME + " WHERE employee_id = :id")
    Single<EmployeeDB> getById(long id);

    @Query("SELECT * FROM " + EmployeeDB.EMPLOYEE_TABLE_NAME + " WHERE last_name = :lastName")
    Single<List<EmployeeDB>> getByLastName(String lastName);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert(List<EmployeeDB> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(EmployeeDB employee);

    @Delete
    int delete(List<EmployeeDB> list);

    @Delete
    int delete(EmployeeDB employee);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(List<EmployeeDB> list);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(EmployeeDB employee);
}
