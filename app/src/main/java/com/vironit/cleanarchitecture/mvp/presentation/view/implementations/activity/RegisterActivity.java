package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.constants.AuthType;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.RegisterPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IRegisterView;

import butterknife.BindView;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity<RegisterPresenter>
        implements IRegisterView {

    @InjectPresenter
    RegisterPresenter mRegisterPresenter;

    @BindView(R.id.et_username)
    EditText etUserName;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.et_password_confirm)
    EditText etPasswordConfirm;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    public int getRootViewResId() {
        return R.id.v_root_register_activity;
    }

    @Override
    protected RegisterPresenter getPresenter() {
        return mRegisterPresenter;
    }

    @OnClick(R.id.btn_already_registered)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.btn_register,
            R.id.btn_facebook_auth, R.id.btn_twitter_auth, R.id.btn_google_auth})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_facebook_auth:
                mRegisterPresenter.socialsAuthButtonPressed(AuthType.FACEBOOK, this);
                break;
            case R.id.btn_twitter_auth:
                mRegisterPresenter.socialsAuthButtonPressed(AuthType.TWITTER, this);
                break;
            case R.id.btn_google_auth:
                mRegisterPresenter.socialsAuthButtonPressed(AuthType.GOOGLE, this);
                break;
            case R.id.btn_register:
                mRegisterPresenter.registerButtonPressed(getText(etUserName),
                        getText(etEmail),
                        getText(etPassword),
                        getText(etPasswordConfirm),
                        this);
                break;
        }
    }

    @Override
    public void startHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    @Override
    public void setCredentials(@Nullable String userName, @Nullable String email) {
        if (!TextUtils.isEmpty(userName)) {
            setUserName(userName);
        }
        if (!TextUtils.isEmpty(email)) {
            setEmail(email);
        }
    }

    private void setUserName(@NonNull String userName) {
        etUserName.setText(userName);
    }

    private void setEmail(@NonNull String email) {
        etEmail.setText(email);
    }

    @NonNull
    private String getText(@NonNull EditText etView) {
        return etView.getText().toString();
    }

    @Override
    public void showSuccessMessage() {
        showDialogMessage(getString(R.string.successful_entry_msg), true);
    }

    @Override
    public void showFailMessage() {
        showDialogMessage(getString(R.string.failed_entry_msg), true);
    }

    @Override
    public void showEmptyFieldMessage() {
        showDialogMessage(getString(R.string.all_fields_must_be_filled), true);
    }
}
