package com.vironit.cleanarchitecture.mvp.presentation.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Article;
import com.vironit.cleanarchitecture.mvp.presentation.adapter.base.BasePaginationRecyclerViewAdapter;
import com.vironit.cleanarchitecture.mvp.presentation.adapter.base.BaseViewHolder;
import com.vironit.cleanarchitecture.util.AppLog;

import butterknife.BindView;

public class NewsFeedAdapter
        extends BasePaginationRecyclerViewAdapter<Article, NewsFeedAdapter.ViewHolder> {

    private Context mContext;
    private RequestOptions mImageRequestOptions;

    public NewsFeedAdapter(@NonNull Context context) {
        AppLog.logObject(this);
        mContext = context;
        mImageRequestOptions = new RequestOptions()
                .fallback(new ColorDrawable(Color.GREEN));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AppLog.logObject(this);
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AppLog.logObject(this);
        Article article = getData().get(position);
        holder.mSourceName.setText(article.getSource().getName());
        Glide.with(mContext)
                .load(article.getUrlToImage())
                .apply(mImageRequestOptions)
                .into(holder.mImageView);
        holder.mTitle.setText(article.getTitle());
        holder.mDescription.setText(article.getDescription());
        holder.mButton.setTag(article.getUrl());
    }

    class ViewHolder extends BaseViewHolder<Article> {

        @BindView(R.id.tv_item_news_source_name)
        TextView mSourceName;
        @BindView(R.id.iv_item_news_image)
        ImageView mImageView;
        @BindView(R.id.tv_item_news_title)
        TextView mTitle;
        @BindView(R.id.tv_item_news_description)
        TextView mDescription;
        @BindView(R.id.btn_item_news_read_more)
        Button mButton;

        public ViewHolder(View itemView) {
            super(itemView);
            AppLog.logObject(this);

            mButton.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse((String) view.getTag()));
                mContext.startActivity(intent);
            });
        }
    }
}
