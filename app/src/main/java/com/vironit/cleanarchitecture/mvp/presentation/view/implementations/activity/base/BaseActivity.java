package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.ResourcesManager;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBaseView;
import com.vironit.cleanarchitecture.util.AppLog;
import com.vironit.cleanarchitecture.util.KeyboardUtil;
import com.vironit.cleanarchitecture.util.ShowDialogUtil;
import com.vironit.cleanarchitecture.util.ShowSnackbarUtil;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import io.reactivex.Scheduler;

public abstract class BaseActivity<P extends BaseAppPresenter> extends MvpAppCompatActivity implements IBaseView {

    @Inject
    @Named(AppConstants.UI_SCHEDULER)
    protected Scheduler mUiScheduler;

    @Inject
    @Named(AppConstants.IO_SCHEDULER)
    protected Scheduler mIoScheduler;

    @Inject
    @Named(AppConstants.COMPUTATION_SCHEDULER)
    protected Scheduler mComputationScheduler;

    @Inject
    protected ResourcesManager mResourcesManager;

    @Nullable
    private Snackbar mSnackbar;

    @Nullable
    private AlertDialog mAlertDialog;

    protected final Handler mHandler = new Handler();

    @LayoutRes
    public abstract int getLayoutResId();

    @IdRes
    public abstract int getRootViewResId();

    protected abstract P getPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppLog.logActivity(this);
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        if (getIntent() != null && getIntent().getExtras() != null) {
            initFromIntentExtras(getIntent().getExtras());
        }
        initBeforeLayoutAttached();
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        initViewsBeforePresenterAttached();
        getMvpDelegate().onAttach();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getMvpDelegate().onAttach();
    }

    @Override
    protected void onStart() {
        AppLog.logActivity(this);
        super.onStart();
    }

    @Override
    protected void onResume() {
        AppLog.logActivity(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        AppLog.logActivity(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        AppLog.logActivity(this);
        hideProgress();
        hideDialogMessage();
        hideMessage();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        AppLog.logActivity(this);
        super.onDestroy();
    }

    @Override
    public void hideKeyboard() {
        KeyboardUtil.hideKeyboard(this);
    }

    @Override
    public void hideDialogMessage() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    @Override
    public void cancelScreen() {
        finish();
    }

    @Override
    public void showAutocloseableMessage(String message) {
        showMessage(message, true, null, null);
    }

    @Override
    public void showDialogMessage(String message, boolean closeable) {
        String title = getResources().getString(R.string.app_name);
        showDialogWithOptions(title, message, null,
                null, null, null, closeable);
    }

    @Override
    public void showDialogWithOptions(@Nullable String title,
                                      @NonNull String message,
                                      @Nullable String positiveOptionMessage,
                                      @Nullable String negativeOptionMessage,
                                      @Nullable DialogInterface.OnClickListener positiveOptionListener,
                                      @Nullable DialogInterface.OnClickListener negativeOptionListener,
                                      boolean cancelable) {
        hideDialogMessage();
        String finalTitle = (title == null ? getString(R.string.app_name) : title);
        mAlertDialog = ShowDialogUtil.showMessageDialog(this, finalTitle, message,
                positiveOptionMessage, negativeOptionMessage,
                positiveOptionListener, negativeOptionListener,
                cancelable);
    }

    @Override
    public void showMessage(@NonNull String message,
                            boolean closeable,
                            @Nullable String actionMessage,
                            @Nullable View.OnClickListener actionListener) {
        hideKeyboard();
        hideMessage();

        @Nullable
        View rootView = findViewById(getRootViewResId());
        if (rootView == null) {
            rootView = getWindow().getDecorView();
        }
        int duration = closeable ? BaseTransientBottomBar.LENGTH_LONG : BaseTransientBottomBar.LENGTH_INDEFINITE;
        mSnackbar = ShowSnackbarUtil.showSnackBar(rootView, this, message, actionMessage, actionListener, duration);
    }

    @Override
    public void hideMessage() {
        if (mSnackbar != null) {
            mSnackbar.dismiss();
        }
    }

    @Override
    public void showProgress() {
        showProgress(null);
    }

    @Override
    public void showProgress(String message) {
        ShowDialogUtil.showProgressDialog(this, message, null);
    }

    @Override
    public void hideProgress() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getPresenter().onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data, this);
    }

    protected void initFromIntentExtras(@NonNull Bundle bundle) {

    }

    protected void initBeforeLayoutAttached() {

    }

    protected void initViewsBeforePresenterAttached() {

    }

    protected String getResourcesString(@StringRes int stringId) {
        return mResourcesManager.getString(stringId);
    }
}
