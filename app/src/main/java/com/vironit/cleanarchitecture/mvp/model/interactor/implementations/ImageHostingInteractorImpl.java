package com.vironit.cleanarchitecture.mvp.model.interactor.implementations;

import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.ImageHostingInteractor;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.images.CloudinaryResponse;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.ImageHostingRepository;

import javax.inject.Inject;

import io.reactivex.Single;

public class ImageHostingInteractorImpl implements ImageHostingInteractor {

    private final ImageHostingRepository mImageHostingRepository;

    @Inject
    ImageHostingInteractorImpl(ImageHostingRepository imageHostingRepository) {
        this.mImageHostingRepository = imageHostingRepository;
    }

    @Override
    public Single<CloudinaryResponse> sendImage(String imageUrl) {
        return mImageHostingRepository.sendImage(imageUrl);
    }
}
