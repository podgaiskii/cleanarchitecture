package com.vironit.cleanarchitecture.mvp.model.interactor.interfaces;

import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.User;

import io.reactivex.Single;

public interface AuthInteractor {

    Single<User> registerUser(@NonNull String userName,
                              @NonNull String email,
                              @NonNull String password);

    Single<User> authorizeUser(@NonNull String userNameOrEmail,
                               @NonNull String password);

    Single<Boolean> setOnlineStatus(@NonNull User user, boolean onlineStatus);

    Single<Boolean> setOnlineStatus(@NonNull String userId, boolean onlineStatus);

    Single<Boolean> setCurrentUserOnlineStatus(boolean onlineStatus);
}
