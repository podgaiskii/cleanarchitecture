package com.vironit.cleanarchitecture.mvp.model.repository.interfaces;

import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.UserAuth;

import io.reactivex.Single;

public interface AuthRepository {

    Single<UserAuth> getUser(@NonNull String usernameOrEmail);

    Single<Boolean> setOnlineStatus(@NonNull String userId, @NonNull Boolean onlineStatus);

    Single<UserAuth> addUser(@NonNull String userName,
                             @NonNull String email,
                             @NonNull String password);
}
