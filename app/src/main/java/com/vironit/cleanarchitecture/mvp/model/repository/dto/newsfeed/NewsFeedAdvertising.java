package com.vironit.cleanarchitecture.mvp.model.repository.dto.newsfeed;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.newsfeed.base.NewsFeedItem;

public class NewsFeedAdvertising implements NewsFeedItem {

    private String imageUrl;
    private String text;

    public NewsFeedAdvertising(String imageUrl, String text) {
        this.imageUrl = imageUrl;
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int getNewsFeedItemType() {
        return NewsFeedItem.TYPE_AD;
    }
}
