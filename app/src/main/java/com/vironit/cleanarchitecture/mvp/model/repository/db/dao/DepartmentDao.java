package com.vironit.cleanarchitecture.mvp.model.repository.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vironit.cleanarchitecture.mvp.model.repository.db.entity.DepartmentDB;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface DepartmentDao {

    @Query("SELECT * FROM " + DepartmentDB.DEPARTMENT_TABLE_NAME)
    Single<DepartmentDB> getAll();

    @Query("SELECT * FROM " + DepartmentDB.DEPARTMENT_TABLE_NAME +
            " WHERE " + DepartmentDB.DEPARTMENT_ID + " = :id")
    Single<DepartmentDB> getById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert(List<DepartmentDB> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(DepartmentDB department);

    @Delete
    int delete(List<DepartmentDB> list);

    @Delete
    int delete(DepartmentDB department);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(List<DepartmentDB> list);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(DepartmentDB department);
}
