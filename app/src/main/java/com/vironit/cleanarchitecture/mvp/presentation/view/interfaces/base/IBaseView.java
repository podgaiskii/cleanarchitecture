package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base;

import com.arellomobile.mvp.MvpView;

public interface IBaseView extends IProgressView, IMessageView, MvpView {

    void hideKeyboard();

    void cancelScreen();
}
