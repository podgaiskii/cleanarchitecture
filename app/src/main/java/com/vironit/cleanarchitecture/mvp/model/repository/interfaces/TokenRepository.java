package com.vironit.cleanarchitecture.mvp.model.repository.interfaces;

import io.reactivex.Single;

public interface TokenRepository {

    Single<Boolean> updateTokenIfNeeded();
}
