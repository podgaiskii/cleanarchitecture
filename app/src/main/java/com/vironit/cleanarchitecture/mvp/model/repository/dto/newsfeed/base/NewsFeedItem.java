package com.vironit.cleanarchitecture.mvp.model.repository.dto.newsfeed.base;

public interface NewsFeedItem {

    int TYPE_POST = 1;
    int TYPE_AD = 2;

    int getNewsFeedItemType();
}
