package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.ProfilePresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.base.BaseFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.view.AvatarView;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IProfileView;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileFragment extends BaseFragment<ProfilePresenter> implements IProfileView {

    @InjectPresenter
    ProfilePresenter mProfilePresenter;

    @BindView(R.id.iv_profile_avatar)
    AvatarView vAvatar;

    @BindView(R.id.tv_profile_name)
    TextView tvName;

    @BindView(R.id.tv_profile_email)
    TextView tvEmail;

    @Override
    protected ProfilePresenter getPresenter() {
        return mProfilePresenter;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_profile;
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @OnClick(R.id.fab)
    public void onFabClicked() {
        showDialogWithOptions(
                getString(R.string.dialog_add_photo_title),
                getString(R.string.dialog_add_photo_message),
                getString(R.string.dialog_add_photo_camera),
                getString(R.string.dialog_add_photo_gallery),
                (dialog, which) -> mProfilePresenter.uploadPhotoFromCamera(this),
                (dialog, which) -> mProfilePresenter.uploadPhotoFromGallery(this),
                true);
    }

    @Override
    public void setProfilePhoto(@NonNull String path) {
        vAvatar.setPhoto(path);
    }

    @Override
    public void setProfilePhoto(@NonNull Uri uri) {
        vAvatar.setPhoto(uri);
    }

    @Override
    public void setProfileName(@Nullable String name) {
        if (name != null) {
            tvName.setText(name);
        } else {
            tvName.setText("BLANK_NAME");
        }
    }

    @Override
    public void setProfileEmail(@Nullable String email) {
        if (email != null) {
            tvEmail.setText(email);
        } else {
            tvEmail.setText("BLANK_EMAIL");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProfilePresenter.setProfileInfo();
        mProfilePresenter.setDefaultProfilePhoto();
    }
}
