package com.vironit.cleanarchitecture.mvp.model.interactor.interfaces;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Data;

import io.reactivex.Single;

public interface NewsInteractor {

    Single<Data> getNews(@NonNull String country,
                         @IntRange(from = 1) int page,
                         @IntRange(from = 1, to = 100) int pageSize);
}
