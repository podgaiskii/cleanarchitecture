package com.vironit.cleanarchitecture.mvp.model.manager.implementations;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.TokenManager;

import java.util.concurrent.atomic.AtomicLong;

import javax.inject.Inject;

public class TokenManagerImpl implements TokenManager {

    @NonNull
    @Inject
    Gson mGson;

    @NonNull
    @Inject
    SharedPreferences mSharedPreferences;

    private static final long TOKEN_LIFETIME_MILLIS = 90 * 60 * 1000;

    @Inject
    TokenManagerImpl(@NonNull Gson gson,
                     @NonNull SharedPreferences sharedPreferences) {
        this.mGson = gson;
        this.mSharedPreferences = sharedPreferences;
    }

    @Nullable
    @Override
    public ApiToken getToken() {
        String tokenJson = mSharedPreferences.getString(AppConstants.TOKEN_SP_KEY, null);
        ApiToken token;
        try {
            token = TextUtils.isEmpty(tokenJson) ? null : mGson.fromJson(tokenJson, ApiToken.class);
        } catch (Exception e) {
            token = null;
        }
        return token;
    }

    @Override
    public void saveToken(AtomicLong creationDate, String accessToken, String refreshToken) {
        ApiToken token = new ApiToken(creationDate, accessToken, refreshToken);
        mSharedPreferences.edit()
                .putString(AppConstants.TOKEN_SP_KEY, mGson.toJson(token))
                .apply();
    }

    @Override
    public void deleteToken() {
        mSharedPreferences.edit()
                .remove(AppConstants.TOKEN_SP_KEY)
                .apply();
    }

    @Override
    public boolean isTokenValid() {
        ApiToken token = getToken();
        return token != null &&
                token.getAccessToken() != null &&
                token.getRefreshToken() != null &&
                System.currentTimeMillis() - token.getCreationDate().get() < TOKEN_LIFETIME_MILLIS;
    }

    public class ApiToken {

        private AtomicLong creationDate;
        private volatile String accessToken;
        private volatile String refreshToken;

        ApiToken(AtomicLong creationDate, String accessToken, String refreshToken) {
            this.creationDate = creationDate;
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
        }

        AtomicLong getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(AtomicLong creationDate) {
            this.creationDate = creationDate;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }
    }
}
