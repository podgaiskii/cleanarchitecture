package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vironit.cleanarchitecture.R;

public class AvatarView extends AppCompatImageView {

    private final float SIZE = getResources().getDimension(R.dimen.default_avatar_size);
    private final float RADIUS = getResources().getDimension(R.dimen.default_radius);
    private final float STROKE_WIDTH = getResources().getDimension(R.dimen.default_stroke_width);

    private Rect rect;
    private Paint shapePaint;
    private Path clipPath;

    public AvatarView(Context context) {
        super(context);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setLayerType(LAYER_TYPE_HARDWARE, null);

        rect = new Rect();
        clipPath = new Path();

        shapePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        shapePaint.setStyle(Paint.Style.STROKE);
        shapePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        shapePaint.setColor(ContextCompat.getColor(getContext(), R.color.color_transparent));
        shapePaint.setStrokeWidth(STROKE_WIDTH);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int screenWidth = MeasureSpec.getSize(widthMeasureSpec);
        int screenHeight = MeasureSpec.getSize(heightMeasureSpec);
        rect.set(0, 0, screenWidth, screenHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Path shapePath = getShapePath();
        canvas.drawPath(shapePath, shapePaint);
        clipPath.addPath(shapePath);
        canvas.clipPath(shapePath);
        super.onDraw(canvas);
    }

    private Path getShapePath() {
        Path path = new Path();
        path.moveTo(RADIUS, 0);
        path.lineTo(SIZE - RADIUS, 0);
        path.lineTo(SIZE, RADIUS);
        path.lineTo(SIZE, SIZE);
        path.lineTo(0, SIZE);
        path.lineTo(0, RADIUS);
        path.quadTo(0, 0, RADIUS, 0);
        path.close();
        return path;
    }

    public void setPhoto(String url) {
        Glide.with(getContext())
                .load(url)
                .apply(getRequestOptions())
                .into(this);
    }

    public void setPhoto(Uri uri) {
        setPhoto(uri.toString());
    }

    private RequestOptions getRequestOptions() {
        return new RequestOptions()
                .centerCrop()
                .override((int) SIZE, (int) SIZE);
    }
}
