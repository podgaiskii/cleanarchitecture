package com.vironit.cleanarchitecture.mvp.model.repository.dto.images;

import com.vironit.cleanarchitecture.BuildConfig;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;

public class CloudinaryRequestBody {

    private File file;
    private String apiKey;
    private String apiSecret;
    private String timestamp;
    private String signature;

    private CloudinaryRequestBody() {
    }

    public CloudinaryRequestBody(String filePath) {
        this.file = new File(filePath);
        init();
    }

    public CloudinaryRequestBody(File file) {
        this.file = file;
        init();
    }

    private void init() {
        this.apiKey = BuildConfig.IMAGE_HOSTING_API_KEY;
        this.apiSecret = BuildConfig.IMAGE_HOSTING_API_SECRET;
        this.timestamp = String.valueOf(System.currentTimeMillis() / 1000L);
        this.signature = calculateSignature();
    }

    private String calculateSignature() {
        return new String(Hex.encodeHex(DigestUtils.sha1(
                "timestamp=" + timestamp + apiSecret)));
    }

    public File getFile() {
        return file;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getSignature() {
        return signature;
    }
}
