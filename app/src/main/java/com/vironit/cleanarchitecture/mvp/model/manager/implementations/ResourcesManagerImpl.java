package com.vironit.cleanarchitecture.mvp.model.manager.implementations;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.ResourcesManager;

import javax.inject.Inject;

public class ResourcesManagerImpl implements ResourcesManager {

    @NonNull
    private final Context mAppContext;

    @Inject
    ResourcesManagerImpl(@NonNull Context appContext) {
        mAppContext = appContext;
    }

    @NonNull
    @Override
    public String getString(@StringRes int stringResId) {
        @Nullable String str = null;
        try {
            str = mAppContext.getString(stringResId);
        } catch (Exception e) {
        }
        return str != null ? str : "";
    }

    @NonNull
    @Override
    public String getString(@StringRes int stringResId, @NonNull Object... formatArgs) {
        @Nullable String str = null;
        try {
            str = mAppContext.getString(stringResId, formatArgs);
        } catch (Exception e) {
        }
        return str != null ? str : "";
    }
}
