package com.vironit.cleanarchitecture.mvp.model.interactor.implementations;

import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.NewsInteractor;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Data;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.NewsRepository;
import com.vironit.cleanarchitecture.util.AppLog;
import com.vironit.cleanarchitecture.util.throwable.ErrorHandlingUtil;

import javax.inject.Inject;

import io.reactivex.Single;

public class NewsInteractorImpl implements NewsInteractor {

    private final NewsRepository mNewsRepository;

    @Inject
    NewsInteractorImpl(NewsRepository newsRepository) {
        AppLog.logObject(this);
        this.mNewsRepository = newsRepository;
    }

    @Override
    public Single<Data> getNews(@NonNull String country, int page, int pageSize) {
        AppLog.logObject(this);
        return mNewsRepository.getNews(country, page, pageSize)
                .onErrorResumeNext(ErrorHandlingUtil::defaultHandle);
    }
}
