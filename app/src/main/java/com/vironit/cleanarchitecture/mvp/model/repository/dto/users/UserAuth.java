package com.vironit.cleanarchitecture.mvp.model.repository.dto.users;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAuth extends User {

    public static final String FIELD_PASSWORD_HASH = "passwordHash";

    @NonNull
    @SerializedName(FIELD_PASSWORD_HASH)
    @Expose
    private String passwordHash;

    public UserAuth() {
        super();
        passwordHash = "";
    }

    public UserAuth(@NonNull String userName,
                    @NonNull String userId,
                    @Nullable String firebaseToken,
                    @NonNull Boolean onlineStatus,
                    @NonNull String userEmail,
                    @NonNull String passwordHash) {
        super(userName, userId, firebaseToken, onlineStatus, userEmail);
        this.passwordHash = passwordHash;
    }

    @NonNull
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@NonNull String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
