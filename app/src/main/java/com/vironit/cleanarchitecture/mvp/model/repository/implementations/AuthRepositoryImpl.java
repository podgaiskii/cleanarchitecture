package com.vironit.cleanarchitecture.mvp.model.repository.implementations;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.DatabaseReference;
import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.User;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.UserAuth;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.AuthRepository;
import com.vironit.cleanarchitecture.util.PasswordHasherUtil;
import com.vironit.cleanarchitecture.util.throwable.DatabaseErrorException;
import com.vironit.cleanarchitecture.util.throwable.UserExistsException;
import com.vironit.cleanarchitecture.util.throwable.UserNotFoundException;

import java.util.NoSuchElementException;

import javax.inject.Inject;
import javax.inject.Named;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Scheduler;
import io.reactivex.Single;

public class AuthRepositoryImpl implements AuthRepository {

    DatabaseReference mDatabaseReference;

    Scheduler mIoScheduler;

    @Inject
    AuthRepositoryImpl(@Named(AppConstants.IO_SCHEDULER) Scheduler ioScheduler,
                       DatabaseReference databaseReference) {
        mDatabaseReference = databaseReference;
        mIoScheduler = ioScheduler;
    }

    @Override
    public Single<UserAuth> getUser(@NonNull String usernameOrEmail) {
        return getUserByName(usernameOrEmail)
                .flatMap(user -> Single.<UserAuth>error(new UserExistsException(user)))
                .onErrorResumeNext(throwable -> isNoSuchElementException(throwable) ?
                        getUserByEmail(usernameOrEmail)
                                .flatMap(user -> Single.<UserAuth>error(new UserExistsException(user)))
                                .onErrorResumeNext(throwable1 -> isNoSuchElementException(throwable1) ?
                                        Single.error(new UserNotFoundException())
                                        : Single.error(throwable1))
                        : Single.error(throwable));
    }

    private static boolean isNoSuchElementException(Throwable throwable) {
        return throwable instanceof NoSuchElementException;
    }

    private Single<UserAuth> getUserByEmail(@NonNull String email) {
        return RxFirebaseDatabase.observeSingleValueEvent(
                mDatabaseReference.orderByChild(User.FIELD_USER_EMAIL).equalTo(email),
                dataSnapshot -> dataSnapshot.getChildren().iterator().next().getValue(UserAuth.class))
                .toSingle()
                .subscribeOn(mIoScheduler);
    }

    private Single<UserAuth> getUserByName(@NonNull String name) {
        return RxFirebaseDatabase.observeSingleValueEvent(
                mDatabaseReference.orderByChild(User.FIELD_USER_NAME).equalTo(name),
                dataSnapshot -> dataSnapshot.getChildren().iterator().next().getValue(UserAuth.class))
                .toSingle()
                .subscribeOn(mIoScheduler);
    }

    @Override
    public Single<Boolean> setOnlineStatus(@NonNull String userId, @NonNull Boolean onlineStatus) {
        return Single.just(mDatabaseReference.push())
                .flatMap(databaseReference -> RxFirebaseDatabase.setValue(
                        mDatabaseReference.child(userId).child(User.FIELD_ONLINE_STATUS), onlineStatus)
                        .toSingle(() -> true))
                .subscribeOn(mIoScheduler);
    }

    @Override
    public Single<UserAuth> addUser(@NonNull String userName,
                                    @NonNull String email,
                                    @NonNull String password) {
        return Single.just(mDatabaseReference.push())
                .flatMap(databaseReference -> {
                    @Nullable String key = databaseReference.getKey();
                    if (key != null) {
                        UserAuth user = new UserAuth(userName, key, null,
                                true, email, PasswordHasherUtil.hash(password));
                        return RxFirebaseDatabase.setValue(mDatabaseReference.child(key), user)
                                .toSingle(() -> user);
                    } else {
                        return Single.error(new DatabaseErrorException());
                    }
                })
                .subscribeOn(mIoScheduler);
    }
}
