package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.constants.HomeFragmentType;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.HomePresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base.BaseFragmentsActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.ChatFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.NewsFeedFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.ProfileFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IHomeView;

import butterknife.BindView;
import dagger.android.AndroidInjector;

public class HomeActivity extends BaseFragmentsActivity<HomePresenter> implements IHomeView {

    @InjectPresenter
    HomePresenter mHomePresenter;

    @BindView(R.id.navigation_bar)
    BottomNavigationView navigationBar;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_home;
    }

    @Override
    public int getRootViewResId() {
        return R.id.v_root_home_activity;
    }

    @Override
    protected HomePresenter getPresenter() {
        return mHomePresenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showFragment(HomeFragmentType.NEWS_FEED);
        navigationBar.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.btn_news_feed:
                    showFragment(HomeFragmentType.NEWS_FEED);
                    break;
                case R.id.btn_chat:
                    showFragment(HomeFragmentType.CHAT);
                    break;
                case R.id.btn_profile:
                    showFragment(HomeFragmentType.PROFILE);
                    break;
            }
            return true;
        });
    }

    @Override
    protected int getContainerViewId() {
        return R.id.l_fragment_container;
    }

    @Override
    public void showFragment(@NonNull HomeFragmentType fragmentType) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (fragmentType) {
            case NEWS_FEED:
                fragment = NewsFeedFragment.newInstance();
                title = getString(R.string.news_feed);
                break;
            case CHAT:
                fragment = ChatFragment.newInstance();
                title = getString(R.string.chat);
                break;
            case PROFILE:
                fragment = ProfileFragment.newInstance();
                title = getString(R.string.profile);
                break;
        }
        setTitle(title);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getContainerViewId(), fragment)
                .commit();
    }
}
