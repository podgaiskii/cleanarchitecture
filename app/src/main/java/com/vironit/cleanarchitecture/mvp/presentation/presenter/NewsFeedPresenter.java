package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.vironit.cleanarchitecture.App;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.NewsInteractor;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BasePaginationPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.INewsFeedView;
import com.vironit.cleanarchitecture.util.AppLog;

import javax.inject.Inject;

@InjectViewState
public class NewsFeedPresenter extends BasePaginationPresenter<INewsFeedView> {

    @Inject
    NewsInteractor mNewsInteractor;

    public NewsFeedPresenter() {
        AppLog.logPresenter(this);
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        AppLog.logPresenter(this);
        super.onFirstViewAttach();
        refreshData();
    }

    @Override
    protected void loadData(int totalItemCount, String lastItemId) {
        AppLog.logPresenter(this);
        super.loadData(totalItemCount, lastItemId);
        loadNews(totalItemCount);
    }

    @Override
    protected int getItemCountPerPage() {
        return 7;
    }

    private void loadNews(int totalItemCount) {
        AppLog.logPresenter(this);
        addPaginationDisposable(mNewsInteractor.getNews("ru",
                calculatePage(totalItemCount),
                getItemCountPerPage())
                .observeOn(mUiScheduler)
                .doOnSuccess(data -> {
                    checkIfNextPageAllowed(data.getArticles());
                    getViewState().addData(data.getArticles());
                })
                .doFinally(() -> getViewState().hidePaginationProgress())
                .subscribe(data -> AppLog.logPresenter(this), this));
    }

    private int calculatePage(int totalItemCount) {
        AppLog.logPresenter(this);
        return totalItemCount / getItemCountPerPage() + 1;
    }
}
