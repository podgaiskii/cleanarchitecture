package com.vironit.cleanarchitecture.mvp.model.repository.implementations;

import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.repository.NewsApiInterface;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Data;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.NewsRepository;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public class NewsRepositoryImpl implements NewsRepository {

    private final NewsApiInterface mNewsApiInterface;
    private final Scheduler mIoScheduler;

    @Inject
    NewsRepositoryImpl(NewsApiInterface newsApiInterface,
                       @Named(AppConstants.IO_SCHEDULER) Scheduler ioScheduler) {
        this.mNewsApiInterface = newsApiInterface;
        this.mIoScheduler = ioScheduler;
    }

    @Override
    public Single<Data> getNews(String countryCode, int page, int pageSize) {
        return mNewsApiInterface.getNews(countryCode, page, pageSize)
                .subscribeOn(mIoScheduler);
    }
}
