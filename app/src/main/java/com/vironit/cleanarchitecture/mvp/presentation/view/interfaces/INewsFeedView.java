package com.vironit.cleanarchitecture.mvp.presentation.view.interfaces;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Article;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBasePaginationView;

@StateStrategyType(OneExecutionStateStrategy.class)
public interface INewsFeedView extends IBasePaginationView, IAddDataList<Article> {

    
}
