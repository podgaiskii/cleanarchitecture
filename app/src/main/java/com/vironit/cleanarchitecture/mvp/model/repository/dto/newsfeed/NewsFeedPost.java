package com.vironit.cleanarchitecture.mvp.model.repository.dto.newsfeed;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.newsfeed.base.NewsFeedItem;

public class NewsFeedPost implements NewsFeedItem {

    @Override
    public int getNewsFeedItemType() {
        return NewsFeedItem.TYPE_POST;
    }
}
