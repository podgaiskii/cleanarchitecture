package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base;

import android.content.Intent;

import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.DummyActivity;

public abstract class BaseDoubleBackActivity<P extends BaseAppPresenter> extends BaseActivity<P> {

    private boolean doubleBackToExitPressedOnce = false;

    private void handleTaskBack() {
        if (doubleBackToExitPressedOnce) {
            startActivity(new Intent(this, DummyActivity.class));
            finish();
            doubleBackToExitPressedOnce = false;
        } else {
            doubleBackToExitPressedOnce = true;
            showAutocloseableMessage(getString(R.string.press_again_to_exit));
            mHandler.postDelayed(() -> doubleBackToExitPressedOnce = false, AppConstants.LONG_DOUBLE_BACK_DELAY);
        }
    }

    @Override
    public void onBackPressed() {
        handleTaskBack();
    }
}
