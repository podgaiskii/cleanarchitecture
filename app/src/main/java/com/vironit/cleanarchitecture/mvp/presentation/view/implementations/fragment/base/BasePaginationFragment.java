package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.base;

import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vironit.cleanarchitecture.R;
import com.vironit.cleanarchitecture.mvp.presentation.adapter.base.BasePaginationRecyclerViewAdapter;
import com.vironit.cleanarchitecture.mvp.presentation.adapter.base.BaseRecyclerViewAdapter;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BasePaginationPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.base.IBasePaginationView;
import com.vironit.cleanarchitecture.util.AppLog;

public abstract class BasePaginationFragment<P extends BasePaginationPresenter>
        extends BaseFragment<P> implements IBasePaginationView {

    @NonNull
    protected abstract SwipeRefreshLayout getSwipeRefreshLayout();

    @NonNull
    protected abstract RecyclerView getRecyclerView();

    @NonNull
    protected abstract BasePaginationRecyclerViewAdapter getRecyclerViewAdapter();

    protected abstract void setRecyclerViewAdapter();

    protected abstract void setLayoutManager();

    private void setOnScrollListener() {
        AppLog.logFragment(this);
        if (getPresenter() != null) {
            getRecyclerView().addOnScrollListener(new RecyclerView.OnScrollListener() {

                private RecyclerView mRecyclerView;
                private BaseRecyclerViewAdapter mRecyclerViewAdapter;

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    AppLog.logObject(this);
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0) {
                        init(recyclerView);
                        getPresenter().loadCheckedData(getItemCount(),
                                getLastVisibleItemPosition(),
                                getLastItemId());
                    }
                }

                private void init(RecyclerView recyclerView) {
                    AppLog.logObject(this);
                    mRecyclerView = recyclerView;
                    mRecyclerViewAdapter = (BaseRecyclerViewAdapter) recyclerView.getAdapter();
                }

                private int getItemCount() {
                    AppLog.logObject(this);
                    return mRecyclerViewAdapter.getRealItemCount();
                }

                private int getLastVisibleItemPosition() {
                    AppLog.logObject(this);
                    return ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                            .findLastVisibleItemPosition();
                }

                private String getLastItemId() {
                    AppLog.logObject(this);
                    if (mRecyclerViewAdapter instanceof BasePaginationRecyclerViewAdapter) {
                        return ((BasePaginationRecyclerViewAdapter) mRecyclerViewAdapter)
                                .getLastItemId();
                    }
                    return null;
                }
            });
        }
    }

    private void setOnRefreshListener() {
        AppLog.logFragment(this);
        int[] colorScheme = {R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark};
        getSwipeRefreshLayout().setColorSchemeResources(colorScheme);
        getSwipeRefreshLayout().setOnRefreshListener(() -> getPresenter().refreshData());
    }

    @Override
    protected void initBeforePresenterAttached() {
        AppLog.logFragment(this);
        super.initBeforePresenterAttached();
        setRecyclerViewAdapter();
        setLayoutManager();
        setOnScrollListener();
        setOnRefreshListener();
    }

    private Runnable mEnableRefreshRunnable = () -> {
        AppLog.logObject(this);
        if (!getSwipeRefreshLayout().isRefreshing()) {
            getSwipeRefreshLayout().setRefreshing(true);
        }
    };

    private Runnable mDisableRefreshRunnable = () -> {
        AppLog.logObject(this);
        if (getSwipeRefreshLayout().isRefreshing()) {
            getSwipeRefreshLayout().setRefreshing(false);
        }
    };

    @Override
    public void removeData() {
        AppLog.logFragment(this);
        getRecyclerViewAdapter().removeData();
    }

    @Override
    public void showPaginationProgress() {
        AppLog.logFragment(this);
        getSwipeRefreshLayout().removeCallbacks(mDisableRefreshRunnable);
        getSwipeRefreshLayout().post(mEnableRefreshRunnable);
    }

    @Override
    public void hidePaginationProgress() {
        AppLog.logFragment(this);
        getSwipeRefreshLayout().removeCallbacks(mEnableRefreshRunnable);
        getSwipeRefreshLayout().post(mDisableRefreshRunnable);
    }

    @Override
    public void onDestroyView() {
        AppLog.logFragment(this);
        hidePaginationProgress();
        super.onDestroyView();
    }
}
