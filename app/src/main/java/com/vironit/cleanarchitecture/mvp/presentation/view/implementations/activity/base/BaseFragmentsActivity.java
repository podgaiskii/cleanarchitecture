package com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.base;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;

import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseFragmentsActivity<P extends BaseAppPresenter>
        extends BaseActivity<P> implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> mAndroidInjector;

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return mAndroidInjector;
    }

    @IdRes
    protected abstract int getContainerViewId();
}
