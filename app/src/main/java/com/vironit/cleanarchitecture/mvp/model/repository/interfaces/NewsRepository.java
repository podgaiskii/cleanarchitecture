package com.vironit.cleanarchitecture.mvp.model.repository.interfaces;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.news.Data;

import io.reactivex.Single;

public interface NewsRepository {

    Single<Data> getNews(String countryCode, int page, int pageSize);
}
