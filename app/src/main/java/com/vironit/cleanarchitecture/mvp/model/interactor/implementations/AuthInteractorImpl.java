package com.vironit.cleanarchitecture.mvp.model.interactor.implementations;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.AuthInteractor;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.User;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.UserAuth;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.AuthRepository;
import com.vironit.cleanarchitecture.util.PasswordHasherUtil;
import com.vironit.cleanarchitecture.util.throwable.UserExistsException;
import com.vironit.cleanarchitecture.util.throwable.UserNotFoundException;

import javax.inject.Inject;

import io.reactivex.Single;

public class AuthInteractorImpl implements AuthInteractor {

    AuthRepository mAuthRepository;

    SharedPreferences mSharedPreferences;

    @Inject
    AuthInteractorImpl(@NonNull AuthRepository authRepository,
                       @NonNull SharedPreferences sharedPreferences) {
        mAuthRepository = authRepository;
        mSharedPreferences = sharedPreferences;
    }

    @Override
    @NonNull
    public Single<User> registerUser(@NonNull String userName,
                                     @NonNull String email,
                                     @NonNull String password) {
        return mAuthRepository.getUser(userName)
                .flatMap(userAuth -> Single.just((User) userAuth))
                .onErrorResumeNext(throwable -> isUserNotFoundException(throwable) ?
                        mAuthRepository.getUser(email)
                                .flatMap(userAuth -> Single.just((User) userAuth))
                                .onErrorResumeNext(throwable1 -> isUserNotFoundException(throwable1) ?
                                        mAuthRepository.addUser(userName, email, password)
                                                .onErrorResumeNext(Single::error)
                                        : Single.error(throwable1))
                        : Single.error(throwable));
    }

    @Override
    @NonNull
    public Single<User> authorizeUser(@NonNull String userNameOrEmail,
                                      @NonNull String password) {
        return mAuthRepository.getUser(userNameOrEmail)
                .flatMap(userAuth -> Single.just((User) userAuth))
                .onErrorResumeNext(throwable -> isUserExistsException(throwable) ?
                        isPasswordMatches(password, getUserFromException(throwable)) ?
                                userAuthSuccessful(throwable)
                                : Single.error(throwable)
                        : Single.error(throwable));
    }

    @Override
    @NonNull
    public Single<Boolean> setOnlineStatus(@NonNull User user, boolean onlineStatus) {
        return mAuthRepository.setOnlineStatus(user.getUserId(), onlineStatus);
    }

    @Override
    @NonNull
    public Single<Boolean> setOnlineStatus(@NonNull String userId, boolean onlineStatus) {
        return mAuthRepository.setOnlineStatus(userId, onlineStatus);
    }

    @Override
    @NonNull
    public Single<Boolean> setCurrentUserOnlineStatus(boolean onlineStatus) {
        return mAuthRepository.setOnlineStatus(getCurrentUserId(), onlineStatus);
    }

    @NonNull
    private String getCurrentUserId() {
        return mSharedPreferences.getString(User.FIELD_USER_ID, "");
    }

    @NonNull
    private Single<User> userAuthSuccessful(Throwable throwable) {
        User user = getUserFromException(throwable);
        mAuthRepository.setOnlineStatus(user.getUserId(), true);
        return Single.just(user);
    }

    private boolean isUserNotFoundException(@NonNull Throwable throwable) {
        return throwable instanceof UserNotFoundException;
    }

    private boolean isUserExistsException(@NonNull Throwable throwable) {
        return throwable instanceof UserExistsException;
    }

    private boolean isPasswordMatches(@NonNull String password,
                                      @NonNull UserAuth user) {
        return user.getPasswordHash().equals(PasswordHasherUtil.hash(password));
    }

    @NonNull
    private UserAuth getUserFromException(Throwable exception) {
        return ((UserExistsException) exception).getUser();
    }
}