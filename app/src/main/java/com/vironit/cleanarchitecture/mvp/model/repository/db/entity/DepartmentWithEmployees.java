package com.vironit.cleanarchitecture.mvp.model.repository.db.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;
import android.support.annotation.Nullable;

import java.util.List;

public class DepartmentWithEmployees {

    @Nullable
    @Embedded
    private DepartmentDB department;

    @Nullable
    @Relation(entity = EmployeeDB.class,
            parentColumn = DepartmentDB.DEPARTMENT_ID,
            entityColumn = DepartmentDB.DEPARTMENT_ID)
    private List<EmployeeDB> employee;

    @Nullable
    public DepartmentDB getDepartment() {
        return department;
    }

    public void setDepartment(@Nullable DepartmentDB department) {
        this.department = department;
    }

    @Nullable
    public List<EmployeeDB> getEmployee() {
        return employee;
    }

    public void setEmployee(@Nullable List<EmployeeDB> employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "DepartmentWithEmployees{" +
                "department=" + department.toString() +
                ", employee=" + employee.toString() +
                '}';
    }
}
