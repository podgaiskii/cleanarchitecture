package com.vironit.cleanarchitecture.mvp.model.repository;

import com.vironit.cleanarchitecture.BuildConfig;
import com.vironit.cleanarchitecture.mvp.model.repository.dto.images.CloudinaryResponse;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ImageHostingApiInterface {

    @Multipart
    @POST(BuildConfig.IMAGE_HOSTING_CLOUD_NAME + "/image/upload")
    Single<CloudinaryResponse> sendImage(@Part MultipartBody.Part image,
                                         @Part MultipartBody.Part apiKey,
                                         @Part MultipartBody.Part timestamp,
                                         @Part MultipartBody.Part signature);
}
