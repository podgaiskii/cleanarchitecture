package com.vironit.cleanarchitecture.mvp.model.repository.dto.users;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    public static final String FIELD_USER_NAME = "userName";
    public static final String FIELD_USER_ID = "userId";
    public static final String FIELD_FIREBASE_TOKEN = "firebaseToken";
    public static final String FIELD_ONLINE_STATUS = "onlineStatus";
    public static final String FIELD_USER_EMAIL = "userEmail";

    @NonNull
    @SerializedName(FIELD_USER_NAME)
    @Expose
    private String userName;

    @NonNull
    @SerializedName(FIELD_USER_ID)
    @Expose
    private String userId;

    @Nullable
    @SerializedName(FIELD_FIREBASE_TOKEN)
    @Expose
    private String firebaseToken;

    @NonNull
    @SerializedName(FIELD_ONLINE_STATUS)
    @Expose
    private Boolean onlineStatus;

    @NonNull
    @SerializedName(FIELD_USER_EMAIL)
    @Expose
    private String userEmail;

    public User() {
        this.userName = "";
        this.userId = "";
        this.firebaseToken = null;
        this.onlineStatus = true;
        this.userEmail = "";
    }

    public User(@NonNull String userName,
                @NonNull String userId,
                @Nullable String firebaseToken,
                @NonNull Boolean onlineStatus,
                @NonNull String userEmail) {
        this.userName = userName;
        this.userId = userId;
        this.firebaseToken = firebaseToken;
        this.onlineStatus = onlineStatus;
        this.userEmail = userEmail;
    }

    @NonNull
    public String getUserName() {
        return userName;
    }

    public void setUserName(@NonNull String userName) {
        this.userName = userName;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    @Nullable
    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(@Nullable String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    @NonNull
    public Boolean getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(@NonNull Boolean onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    @NonNull
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(@NonNull String userEmail) {
        this.userEmail = userEmail;
    }
}
