package com.vironit.cleanarchitecture.mvp.model.repository.implementations;

import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.TokenManager;
import com.vironit.cleanarchitecture.mvp.model.repository.AuthApiInterface;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.TokenRepository;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public class TokenRepositoryImpl implements TokenRepository {

    private final TokenManager mTokenManager;
    private final Scheduler mIoScheduler;
    private final AuthApiInterface mAuthApiInterface;

    private final AtomicBoolean mIsTokenUpdating = new AtomicBoolean(false);
    private static final long DELAY = 100;

    @Inject
    TokenRepositoryImpl(TokenManager tokenManager,
                        @Named(AppConstants.IO_SCHEDULER) Scheduler ioScheduler,
                        AuthApiInterface authApiInterface) {
        mTokenManager = tokenManager;
        mIoScheduler = ioScheduler;
        mAuthApiInterface = authApiInterface;
    }

    @Override
    public Single<Boolean> updateTokenIfNeeded() {
        if (mTokenManager.isTokenValid()) {
            return Single.just(true);
        } else {
            return updateToken();
        }
    }

    private Single<Boolean> updateToken() {
        if (mIsTokenUpdating.get()) {
            return Single.timer(DELAY, TimeUnit.MILLISECONDS, mIoScheduler)
                    .flatMap(aLong -> updateTokenIfNeeded());
        } else {
            return Single.fromCallable(() -> {
                mIsTokenUpdating.set(true);
                return true;
                // todo return mApiAuthInterface;
            })
                    .flatMap(apiAuthInterface -> Single.just(true)
                            //todo get from network
                    )
                    .map(newToken -> true
                            //todo save new updated token to manager
                    )
                    .map(aBoolean -> mIsTokenUpdating.getAndSet(false));
        }
    }
}
