package com.vironit.cleanarchitecture.mvp.model.manager.implementations.network;

import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.BuildConfig;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class NewsHeaderInterceptor implements Interceptor {

    @Inject
    NewsHeaderInterceptor() {
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder();
        requestBuilder.header("Authorization", "Bearer " + BuildConfig.NEWS_API_KEY);
        return chain.proceed(requestBuilder.build());
    }
}
