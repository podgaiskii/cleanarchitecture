package com.vironit.cleanarchitecture.mvp.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BaseAppPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.view.interfaces.IMainView;

@InjectViewState
public class MainPresenter extends BaseAppPresenter<IMainView> {

    public void showProgress() {
        getViewState().showProgress();
    }

    public void showProgressWithMessage(String message) {
        getViewState().showProgress(message);
    }

    public void showMessage(String message) {
        getViewState().showMessage(message, true, null, null);
    }

    public void showDialogMessage(String message) {
        getViewState().showDialogMessage(message, true);
    }

    public void showDialogWithOptions(String title, String message) {
        getViewState().showDialogWithOptions(title, message,
                "ok", "cancel",
                null, null, true);
    }
}
