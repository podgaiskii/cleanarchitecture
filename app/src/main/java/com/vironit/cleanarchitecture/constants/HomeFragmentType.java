package com.vironit.cleanarchitecture.constants;

public enum HomeFragmentType {

    NEWS_FEED, CHAT, PROFILE

}
