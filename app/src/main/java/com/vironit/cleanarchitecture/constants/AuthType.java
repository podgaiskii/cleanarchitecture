package com.vironit.cleanarchitecture.constants;

public enum AuthType {

    APP, FACEBOOK, TWITTER, GOOGLE

}
