package com.vironit.cleanarchitecture.constants;

public interface AppConstants {

    int LONG_DOUBLE_BACK_DELAY = 2000;
    int SPLASH_SCREEN_DELAY = 1000;

    String COMPUTATION_SCHEDULER = "COMPUTATION_SCHEDULER";
    String UI_SCHEDULER = "UI_SCHEDULER";
    String IO_SCHEDULER = "IO_SCHEDULER";

    String NEWS_CLIENT = "NEWS_CLIENT";
    String IMAGE_HOSTING_CLIENT = "IMAGE_HOSTING_CLIENT";
    String AUTH_CLIENT = "AUTH_CLIENT";

    long CONNECT_TIMEOUT_SECONDS = 15;
    long READ_WRITE_TIMEOUT_SECONDS = 20;

    long MAX_CACHE_SIZE = 20 * 1024 * 1024;

    int NO_STRING_RES = -1;

    String TOKEN_SP_KEY = "token";

    int DEFAULT_ITEM_COUNT_PER_PAGE = 20;

    String FIREBASE_USERS_JSON = "USERS";
}
