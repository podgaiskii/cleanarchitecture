package com.vironit.cleanarchitecture.util.throwable;

import android.support.annotation.Nullable;

import com.vironit.cleanarchitecture.R;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Single;

public abstract class ErrorHandlingUtil {

    public static <T> Single<T> defaultHandle(@Nullable Throwable throwable) {
        if (throwable == null) {
            return Single.error(new CleanArchitectureBaseException(R.string.unknown_error));
        } else if (throwable instanceof CleanArchitectureBaseException) {
            return Single.error(throwable);
        } else if (isNoNetworkException(throwable)) {
            return Single.error(new CleanArchitectureBaseException(R.string.no_network));
        } else {
            return Single.error(new CleanArchitectureBaseException(R.string.unknown_error));
        }
    }

    private static boolean isNoNetworkException(@Nullable Throwable throwable) {
        return throwable != null
                && (throwable instanceof SocketTimeoutException
                || throwable instanceof ConnectException
                || throwable instanceof UnknownHostException);
    }
}
