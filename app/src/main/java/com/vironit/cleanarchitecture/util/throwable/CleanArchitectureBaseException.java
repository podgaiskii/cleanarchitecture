package com.vironit.cleanarchitecture.util.throwable;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

public class CleanArchitectureBaseException extends Exception {

    @Nullable
    ErrorStatus mErrorStatus;

    @Nullable
    @StringRes
    private final Integer stringResId;

    public CleanArchitectureBaseException(@NonNull ErrorStatus errorStatus) {
        this.mErrorStatus = errorStatus;
        stringResId = null;
    }

    public CleanArchitectureBaseException(@NonNull Integer stringResId) {
        this.stringResId = stringResId;
    }

    public CleanArchitectureBaseException(@NonNull ErrorStatus errorStatus,
                                          @NonNull Throwable cause) {
        super(cause);
        this.mErrorStatus = errorStatus;
        stringResId = null;
    }

    public CleanArchitectureBaseException(@NonNull Integer stringResId,
                                          @NonNull Throwable cause) {
        super(cause);
        this.stringResId = stringResId;
    }

    public CleanArchitectureBaseException(@NonNull ErrorStatus errorStatus,
                                          @NonNull Throwable cause,
                                          @NonNull String message) {
        super(message, cause);
        this.mErrorStatus = errorStatus;
        stringResId = null;
    }

    public CleanArchitectureBaseException(@NonNull Integer stringResId,
                                          @NonNull Throwable cause,
                                          @NonNull String message) {
        super(message, cause);
        this.stringResId = stringResId;
    }


    @Nullable
    public ErrorStatus getErrorStatus() {
        return mErrorStatus;
    }

    @Nullable
    @StringRes
    public Integer getStringResId() {
        return stringResId;
    }
}
