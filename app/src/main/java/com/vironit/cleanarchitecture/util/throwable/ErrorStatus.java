package com.vironit.cleanarchitecture.util.throwable;

import android.support.annotation.StringRes;

import com.vironit.cleanarchitecture.R;

import static com.vironit.cleanarchitecture.constants.AppConstants.NO_STRING_RES;

public enum ErrorStatus {

    USER_EXISTS(R.string.user_exists, 1),
    USER_NOT_FOUND(R.string.user_not_found, 2),
    DATABASE_ERROR(R.string.database_error, 3);

    final int mServerErrorCode;
    @StringRes final int mStringResId;

    ErrorStatus(@StringRes int stringResId, int serverErrorCode) {
        this.mStringResId = stringResId;
        this.mServerErrorCode = serverErrorCode;
    }

    public boolean hasStringMessage() {
        return this.mStringResId != NO_STRING_RES;
    }

    @StringRes
    public int getStringResId() {
        return mStringResId;
    }
}
