package com.vironit.cleanarchitecture.util;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

public abstract class PasswordHasherUtil {

    public static String hash(String password) {
        return new String(Hex.encodeHex(DigestUtils.sha1(password)));
    }
}
