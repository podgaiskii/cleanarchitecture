package com.vironit.cleanarchitecture.util;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.vironit.cleanarchitecture.BuildConfig;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.base.BasePresenter;

public abstract class AppLog {

    private static final int DEFAULT_STACK_TRACE_ELEMENT = 5;

    public static void logPresenter(@NonNull BasePresenter presenter) {
        logObject(presenter);
    }

    public static void logPresenter(@NonNull BasePresenter presenter,
                                    @Nullable String message) {
        logObject(presenter.getClass(), message);
    }

    public static void logPresenter(@NonNull BasePresenter presenter,
                                    @Nullable String message,
                                    @Nullable Throwable throwable) {
        if (isLogEnabled()) {
            Log.e(getAppTag(), createMessageFromThrowableAndMessage(throwable, message));
        }
    }

    public static void logActivity(@NonNull Activity activity) {
        if (isLogEnabled()) {
            Log.e(getAppTag(), getInfo(activity));
        }
    }

    public static void logFragment(@NonNull Fragment fragment) {
        if (isLogEnabled()) {
            Log.e(getAppTag(), getInfo(fragment));
        }
    }

    private static String getMethodName(int stackTraceElement) {
        try {
            return Thread.currentThread().getStackTrace()[stackTraceElement].getMethodName();
        } catch (Exception e) {
            return "Unknown method";
        }
    }

    private static String getClassName(Object o) {
        return o.getClass().getName();
    }

    private static String getInfo(Object o) {
        int stackTraceElement = DEFAULT_STACK_TRACE_ELEMENT;
        if (o instanceof BasePresenter) {
            stackTraceElement += 1;
        }
        return getClassName(o) + "." + getMethodName(stackTraceElement) + "()";
    }

    private static boolean isLogEnabled() {
        return BuildConfig.DEBUG;
    }

    private static String getAppTag() {
        return "AppLog";
    }

    @NonNull
    private static String createMessage(@Nullable String message) {
        return TextUtils.isEmpty(message) ? "Empty message" : message;
    }

    @NonNull
    public static String createMessageFromThrowable(@Nullable Throwable throwable) {
        return throwable != null ?
                (throwable.getClass().getName() + " " + createMessage(throwable.getMessage())) :
                "Nullable throwable";
    }

    @NonNull
    public static String createMessageFromThrowableAndMessage(@Nullable Throwable throwable,
                                                              @Nullable String message) {
        return message + " " + createMessageFromThrowable(throwable);
    }

    public static void logObject(Class clazz, @Nullable String message) {
        if (isLogEnabled()) {
            int stackTraceElement = DEFAULT_STACK_TRACE_ELEMENT;
            if (clazz.isAssignableFrom(BasePresenter.class)) {
                stackTraceElement += 1;
            }
            Log.e(getAppTag(),
                    clazz.getSimpleName() + '.' +
                            getMethodName(stackTraceElement) + ' ' +
                            createMessage(message));
        }
    }

    public static void logObject(Object object) {
        if (isLogEnabled()) {
            Log.e(getAppTag(), getInfo(object));
        }
    }
}
