package com.vironit.cleanarchitecture.util;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.User;

public abstract class SharedPreferencesUtil {

    @NonNull
    public static User getCurrentUser(@NonNull SharedPreferences sharedPreferences) {
        return new User(sharedPreferences.getString(User.FIELD_USER_NAME, ""),
                sharedPreferences.getString(User.FIELD_USER_ID, ""),
                sharedPreferences.getString(User.FIELD_FIREBASE_TOKEN, null),
                true,
                sharedPreferences.getString(User.FIELD_USER_EMAIL, ""));
    }

    public static void saveCurrentUser(@NonNull SharedPreferences sharedPreferences,
                                       @NonNull User user) {
        sharedPreferences.edit()
                .putString(User.FIELD_USER_NAME, user.getUserName())
                .putString(User.FIELD_USER_ID, user.getUserId())
                .putString(User.FIELD_FIREBASE_TOKEN, user.getFirebaseToken())
                .putString(User.FIELD_USER_EMAIL, user.getUserEmail())
                .apply();
    }

    public static void clearCurrentUser(@NonNull SharedPreferences sharedPreferences) {
        sharedPreferences.edit()
                .putString(User.FIELD_USER_NAME, null)
                .putString(User.FIELD_USER_ID, null)
                .putString(User.FIELD_FIREBASE_TOKEN, null)
                .putString(User.FIELD_USER_EMAIL, null)
                .apply();
    }
}
