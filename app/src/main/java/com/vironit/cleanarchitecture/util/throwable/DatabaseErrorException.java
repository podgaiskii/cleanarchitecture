package com.vironit.cleanarchitecture.util.throwable;

public class DatabaseErrorException extends CleanArchitectureBaseException {

    public DatabaseErrorException() {
        super(ErrorStatus.DATABASE_ERROR);
    }
}
