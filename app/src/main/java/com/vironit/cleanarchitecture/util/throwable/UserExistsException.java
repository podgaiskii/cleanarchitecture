package com.vironit.cleanarchitecture.util.throwable;

import android.support.annotation.NonNull;

import com.vironit.cleanarchitecture.mvp.model.repository.dto.users.UserAuth;

public class UserExistsException extends CleanArchitectureBaseException {

    private UserAuth mUser;

    public UserExistsException(@NonNull UserAuth user) {
        super(ErrorStatus.USER_EXISTS);
        mUser = user;
    }

    public UserAuth getUser() {
        return mUser;
    }
}
