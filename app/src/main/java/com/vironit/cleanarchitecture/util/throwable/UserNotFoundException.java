package com.vironit.cleanarchitecture.util.throwable;

public class UserNotFoundException extends CleanArchitectureBaseException {

    public UserNotFoundException() {
        super(ErrorStatus.USER_NOT_FOUND);
    }
}
