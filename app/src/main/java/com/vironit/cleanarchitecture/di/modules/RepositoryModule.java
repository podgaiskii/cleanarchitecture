package com.vironit.cleanarchitecture.di.modules;

import com.vironit.cleanarchitecture.mvp.model.repository.implementations.AuthRepositoryImpl;
import com.vironit.cleanarchitecture.mvp.model.repository.implementations.ImageHostingRepositoryImpl;
import com.vironit.cleanarchitecture.mvp.model.repository.implementations.NewsRepositoryImpl;
import com.vironit.cleanarchitecture.mvp.model.repository.implementations.TokenRepositoryImpl;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.AuthRepository;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.ImageHostingRepository;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.NewsRepository;
import com.vironit.cleanarchitecture.mvp.model.repository.interfaces.TokenRepository;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public interface RepositoryModule {

    @Binds
    @Singleton
    NewsRepository provideNewsRepository(NewsRepositoryImpl newsRepository);

    @Binds
    @Singleton
    ImageHostingRepository provideImageHostingRepository(
            ImageHostingRepositoryImpl imageHostingRepository);

    @Binds
    @Singleton
    AuthRepository provideAuthRepository(AuthRepositoryImpl authRepository);

    @Binds
    @Singleton
    TokenRepository provideTokenRepository(TokenRepositoryImpl tokenRepository);
}
