package com.vironit.cleanarchitecture.di.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.repository.db.CleanArchitectureDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    CleanArchitectureDatabase provideCleanArchitectureDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                                    CleanArchitectureDatabase.class,
                                    "CleanArchitectureDatabase.db")
                .build();
    }

    @Provides
    @Singleton
    DatabaseReference provideDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference().child(AppConstants.FIREBASE_USERS_JSON);
    }
}
