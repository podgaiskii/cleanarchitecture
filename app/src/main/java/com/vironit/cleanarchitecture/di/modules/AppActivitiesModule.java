package com.vironit.cleanarchitecture.di.modules;

import com.vironit.cleanarchitecture.di.annotations.ActivityScope;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.AuthActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.HomeActivity;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.activity.RegisterActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = {AndroidSupportInjectionModule.class})
public interface AppActivitiesModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = ActivityModule.class)
    AuthActivity authActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = ActivityModule.class)
    RegisterActivity registerActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = ActivityModule.class)
    HomeActivity homeActivityInjector();

}
