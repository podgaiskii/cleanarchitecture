package com.vironit.cleanarchitecture.di.components;

import com.vironit.cleanarchitecture.di.annotations.ActivityScope;
import com.vironit.cleanarchitecture.di.modules.ActivityModule;

import dagger.Component;

@ActivityScope
@Component(modules = {ActivityModule.class}, dependencies = {AppComponent.class})
public interface ActivityComponent {


}
