package com.vironit.cleanarchitecture.di.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.vironit.cleanarchitecture.BuildConfig;
import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.manager.implementations.network.LiviaHeaderInterceptor;
import com.vironit.cleanarchitecture.mvp.model.manager.implementations.network.NewsHeaderInterceptor;
import com.vironit.cleanarchitecture.util.AppLog;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {

    @Provides
    @Singleton
    @Named(AppConstants.NEWS_CLIENT)
    OkHttpClient provideNewsOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                         Cache cache,
                                         NewsHeaderInterceptor newsHeaderInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(AppConstants.CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(AppConstants.READ_WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(AppConstants.READ_WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .cache(cache)
                .addInterceptor(newsHeaderInterceptor);
        if (BuildConfig.IS_LOG_ENABLED) {
            builder.addInterceptor(httpLoggingInterceptor);
        }
        return builder.build();
    }

    @Provides
    @Singleton
    @Named(AppConstants.IMAGE_HOSTING_CLIENT)
    OkHttpClient provideImageHostingOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                                 Cache cache) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(AppConstants.CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(AppConstants.READ_WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(AppConstants.READ_WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .cache(cache);
        if (BuildConfig.IS_LOG_ENABLED) {
            builder.addInterceptor(httpLoggingInterceptor);
        }
        return builder.build();
    }

    @Provides
    @Singleton
    @Named(AppConstants.AUTH_CLIENT)
    OkHttpClient provideAuthOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                         Cache cache,
                                         LiviaHeaderInterceptor liviaHeaderInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(AppConstants.CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(AppConstants.READ_WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(AppConstants.READ_WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .cache(cache)
                .addInterceptor(liviaHeaderInterceptor);
        if (BuildConfig.IS_LOG_ENABLED) {
            builder.addInterceptor(httpLoggingInterceptor);
        }
        return builder.build();
    }

    @Provides
    @Singleton
    Cache provideCache(Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "responses");
        return new Cache(httpCacheDirectory, AppConstants.MAX_CACHE_SIZE);
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor(message -> AppLog.logObject(
                        HttpLoggingInterceptor.class, message));
        httpLoggingInterceptor.setLevel(BuildConfig.IS_LOG_ENABLED ?
                HttpLoggingInterceptor.Level.HEADERS : HttpLoggingInterceptor.Level.NONE);
        return httpLoggingInterceptor;
    }

    @Provides
    @Singleton
    @Named(AppConstants.NEWS_CLIENT)
    Retrofit provideNewsRetrofit(
            @Named(AppConstants.NEWS_CLIENT) OkHttpClient okHttpClient,
            RxJava2CallAdapterFactory rxJava2CallAdapterFactory,
            GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .baseUrl(BuildConfig.NEWS_API_URL)
                .build();
    }

    @Provides
    @Singleton
    @Named(AppConstants.IMAGE_HOSTING_CLIENT)
    Retrofit provideImageHostingRetrofit(
            @Named(AppConstants.IMAGE_HOSTING_CLIENT) OkHttpClient okHttpClient,
            RxJava2CallAdapterFactory rxJava2CallAdapterFactory,
            GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .baseUrl(BuildConfig.IMAGE_HOSTING_API_URL)
                .build();
    }

    @Provides
    @Singleton
    @Named(AppConstants.AUTH_CLIENT)
    Retrofit provideAuthRetrofit(
            @Named(AppConstants.AUTH_CLIENT) OkHttpClient okHttpClient,
            RxJava2CallAdapterFactory rxJava2CallAdapterFactory,
            GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .baseUrl(BuildConfig.AUTH_API_URL)
                .build();
    }

    @Provides
    @Singleton
    RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }
}
