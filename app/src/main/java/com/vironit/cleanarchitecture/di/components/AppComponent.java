package com.vironit.cleanarchitecture.di.components;

import com.vironit.cleanarchitecture.App;
import com.vironit.cleanarchitecture.di.modules.ApiModule;
import com.vironit.cleanarchitecture.di.modules.AppActivitiesModule;
import com.vironit.cleanarchitecture.di.modules.AppFragmentsModule;
import com.vironit.cleanarchitecture.di.modules.ApplicationModule;
import com.vironit.cleanarchitecture.di.modules.DatabaseModule;
import com.vironit.cleanarchitecture.di.modules.InteractorModule;
import com.vironit.cleanarchitecture.di.modules.ManagerModule;
import com.vironit.cleanarchitecture.di.modules.ParceModule;
import com.vironit.cleanarchitecture.di.modules.RepositoryModule;
import com.vironit.cleanarchitecture.di.modules.RetrofitModule;
import com.vironit.cleanarchitecture.di.modules.SchedulersModule;
import com.vironit.cleanarchitecture.di.modules.SocialNetworksModule;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.AuthPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.ChatPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.HomePresenter;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.NewsFeedPresenter;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.ProfilePresenter;
import com.vironit.cleanarchitecture.mvp.presentation.presenter.RegisterPresenter;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {
        ApiModule.class,
        AppActivitiesModule.class,
        AppFragmentsModule.class,
        ApplicationModule.class,
        DatabaseModule.class,
        InteractorModule.class,
        ManagerModule.class,
        ParceModule.class,
        RepositoryModule.class,
        RetrofitModule.class,
        SchedulersModule.class,
        SocialNetworksModule.class})
public interface AppComponent {

    void inject(App app);

    void inject(AuthPresenter presenter);

    void inject(RegisterPresenter presenter);

    void inject(HomePresenter presenter);

    void inject(NewsFeedPresenter presenter);

    void inject(ChatPresenter presenter);

    void inject(ProfilePresenter presenter);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder appContext(App app);

        AppComponent build();
    }

}
