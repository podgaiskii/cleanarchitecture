package com.vironit.cleanarchitecture.di.modules;

import com.vironit.cleanarchitecture.constants.AppConstants;
import com.vironit.cleanarchitecture.mvp.model.repository.AuthApiInterface;
import com.vironit.cleanarchitecture.mvp.model.repository.ImageHostingApiInterface;
import com.vironit.cleanarchitecture.mvp.model.repository.NewsApiInterface;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApiModule {

    @Provides
    @Singleton
    NewsApiInterface provideNewsApiInterface(
            @Named(AppConstants.NEWS_CLIENT) Retrofit retrofit) {
        return retrofit.create(NewsApiInterface.class);
    }

    @Provides
    @Singleton
    ImageHostingApiInterface provideImageHostingApiInterface(
            @Named(AppConstants.IMAGE_HOSTING_CLIENT) Retrofit retrofit) {
        return retrofit.create(ImageHostingApiInterface.class);
    }

    @Provides
    @Singleton
    AuthApiInterface provideAuthApiInterface(
            @Named(AppConstants.AUTH_CLIENT) Retrofit retrofit) {
        return retrofit.create(AuthApiInterface.class);
    }
}
