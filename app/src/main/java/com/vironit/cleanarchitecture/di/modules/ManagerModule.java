package com.vironit.cleanarchitecture.di.modules;

import com.vironit.cleanarchitecture.mvp.model.manager.implementations.ResourcesManagerImpl;
import com.vironit.cleanarchitecture.mvp.model.manager.implementations.TokenManagerImpl;
import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.ResourcesManager;
import com.vironit.cleanarchitecture.mvp.model.manager.interfaces.TokenManager;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public interface ManagerModule {

    @Binds
    @Singleton
    ResourcesManager provideResourcesManager(ResourcesManagerImpl resourcesManager);

    @Binds
    @Singleton
    TokenManager provideTokenManager(TokenManagerImpl tokenManager);
}
