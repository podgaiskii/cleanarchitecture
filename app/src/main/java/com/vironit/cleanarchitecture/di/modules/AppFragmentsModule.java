package com.vironit.cleanarchitecture.di.modules;

import com.vironit.cleanarchitecture.di.annotations.ActivityScope;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.ChatFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.NewsFeedFragment;
import com.vironit.cleanarchitecture.mvp.presentation.view.implementations.fragment.ProfileFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = {AndroidSupportInjectionModule.class})
public interface AppFragmentsModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = FragmentModule.class)
    NewsFeedFragment newsFeedFragmentInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = FragmentModule.class)
    ChatFragment chatFragmentInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = FragmentModule.class)
    ProfileFragment profileFragmentInjector();

}
