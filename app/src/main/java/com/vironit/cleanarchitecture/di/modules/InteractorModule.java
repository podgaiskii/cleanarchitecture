package com.vironit.cleanarchitecture.di.modules;

import com.vironit.cleanarchitecture.mvp.model.interactor.implementations.AuthInteractorImpl;
import com.vironit.cleanarchitecture.mvp.model.interactor.implementations.ImageHostingInteractorImpl;
import com.vironit.cleanarchitecture.mvp.model.interactor.implementations.NewsInteractorImpl;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.AuthInteractor;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.ImageHostingInteractor;
import com.vironit.cleanarchitecture.mvp.model.interactor.interfaces.NewsInteractor;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public interface InteractorModule {

    @Binds
    @Singleton
    NewsInteractor provideNewsInteractor(NewsInteractorImpl newsInteractor);

    @Binds
    @Singleton
    ImageHostingInteractor provideImageHostingInteractor(
            ImageHostingInteractorImpl imageHostingInteractor);

    @Binds
    @Singleton
    AuthInteractor provideAuthInteractor(AuthInteractorImpl authInteractor);
}
