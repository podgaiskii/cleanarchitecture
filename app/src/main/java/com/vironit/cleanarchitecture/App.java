package com.vironit.cleanarchitecture;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;

import com.facebook.stetho.Stetho;
import com.twitter.sdk.android.core.Twitter;
import com.vironit.cleanarchitecture.di.components.AppComponent;
import com.vironit.cleanarchitecture.di.components.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasBroadcastReceiverInjector;
import dagger.android.HasServiceInjector;

public class App extends Application implements HasActivityInjector, HasServiceInjector, HasBroadcastReceiverInjector {

    private static AppComponent sAppComponent;

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Inject
    DispatchingAndroidInjector<BroadcastReceiver> broadcastReceiverDispatchingAndroidInjector;

    @Inject
    DispatchingAndroidInjector<Service> serviceDispatchingAndroidInjector;


    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<BroadcastReceiver> broadcastReceiverInjector() {
        return broadcastReceiverDispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceDispatchingAndroidInjector;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger2();
        Twitter.initialize(this);
        initStetho();
    }

    private void initDagger2() {
        sAppComponent = DaggerAppComponent.builder()
                .appContext(this)
                .build();
        sAppComponent.inject(this);
    }

    private void initStetho() {
        Stetho.initializeWithDefaults(this);
    }
}
